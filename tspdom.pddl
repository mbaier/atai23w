(define (domain tsp-strips)
  (:requirements :strips :fluents)
  (:predicates (in ?x) (visited ?x) (not-visited ?x)
	       (connected ?x ?y))

  (:functions (total-cost) - number
              (distance ?x ?y)
  )

  (:action go
    :parameters (?x ?y)
    :precondition (and (in ?x) (not-visited ?y)
		       (connected ?x ?y))
    :effect (and (not (in ?x)) (in ?y) (visited ?y) (not (not-visited ?y)) (increase (total-cost) (distance ?x ?y)))  )

  

)