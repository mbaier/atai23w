import {Component, Input} from '@angular/core';

interface IPlanEntry {
  startTime: number;
  endTime: number;
  task: string;
}

@Component({
  selector: 'app-production',
  templateUrl: './production.component.html',
  styleUrls: ['./production.component.scss']
})
export class ProductionComponent {
  @Input()
  public plan: string | undefined;

  @Input()
  public planTime: number | undefined;

  @Input()
  public timeStamp: number | undefined;

  public getPlanTime() {
    if (this.planTime == undefined) {
      return 0;
    }
    return this.planTime * 1000;
  }

  public getTimeStamp() {
    if (this.timeStamp == undefined) {
      return 0;
    }
    return this.timeStamp * 1000;
  }


  public splitPlan(): IPlanEntry[] {
    if (this.plan === null) {
      return [];
    }
    return this.plan!.split("]").map(x => {
      const firstSpace = x.indexOf(" ");
      const lastSpace = x.lastIndexOf(" ");
      console.log("Test :", x.substring(lastSpace + 1, x.length));
      return {
        startTime: this.getPlanTime() + parseInt(x.substring(0, firstSpace)) * 1000,
        endTime: this.getPlanTime() + parseInt(x.substring(0, firstSpace)) * 1000 + parseInt(x.substring(lastSpace + 2, x.length)) * 1000,
        task: x.substring(firstSpace, lastSpace)
      }
    });
  }
}
