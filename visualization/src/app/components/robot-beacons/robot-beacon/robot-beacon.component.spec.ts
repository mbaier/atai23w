import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RobotBeaconComponent } from './robot-beacon.component';

describe('RobotBeaconComponent', () => {
  let component: RobotBeaconComponent;
  let fixture: ComponentFixture<RobotBeaconComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [RobotBeaconComponent]
    });
    fixture = TestBed.createComponent(RobotBeaconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
