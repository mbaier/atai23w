import {Component, Input, OnInit} from '@angular/core';
import {mergeMap, of, timer} from "rxjs";

export interface RobotBeacon {
  localTimestamp: number;
  robotName: string;
  teamName: string;
  task: string;
  robotId: number;
}

@Component({
  selector: 'app-robot-beacon',
  templateUrl: './robot-beacon.component.html',
  styleUrls: ['./robot-beacon.component.scss']
})
export class RobotBeaconComponent implements OnInit{
  @Input() beacon!: RobotBeacon;
  @Input() id!: number;
  public seconds: number = 0;

  constructor() {
  }

  ngOnInit() {
    timer(0, 1000).pipe(
      mergeMap(_ => of((this.seconds = Date.now() - this.beacon?.localTimestamp) / 1000))
    ).subscribe();

  }

}
