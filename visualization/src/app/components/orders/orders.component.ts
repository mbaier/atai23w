import {Component, Input} from '@angular/core';
import {IOrder} from "../../services/endpoint.service";

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent {
  @Input()
  public orders: IOrder[] | undefined;
}
