import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ExplorationComponent } from './components/exploration/exploration.component';
import {HttpClientModule} from "@angular/common/http";
import {MatTableModule} from "@angular/material/table";
import { RobotBeaconsComponent } from './components/robot-beacons/robot-beacons.component';
import {CommonModule} from "@angular/common";
import { RobotBeaconComponent } from './components/robot-beacons/robot-beacon/robot-beacon.component';
import { ProductionComponent } from './components/production/production.component';
import { OrdersComponent } from './components/orders/orders.component';
import { ToMsPipe } from './pipes/to-ms.pipe';

@NgModule({
  declarations: [
    AppComponent,
    ExplorationComponent,
    RobotBeaconsComponent,
    RobotBeaconComponent,
    ProductionComponent,
    OrdersComponent,
    ToMsPipe
  ],
  imports: [
    BrowserModule,
    CommonModule,
    AppRoutingModule,
    HttpClientModule,
    MatTableModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
