package com.visualization.domain;

import com.rcll.domain.GamePhase;
import com.rcll.domain.TeamColor;

public interface VisualizationGameState {
    String getTeamCyan();
    String getTeamMagenta();
    long getPointsCyan();
    long getPointsMagenta();
    long getGameTimeNanoSeconds();
    com.rcll.domain.GamePhase getState();
    GamePhase getPhase();
    TeamColor getGripsColor();
}
