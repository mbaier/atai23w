package com.shared.domain;

public enum ZoneExplorationState {UNEXPLORED, EXPLORED, SCHEDULED}