// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: GripsStartPositions.proto

package org.robocup_logistics.llsf_msgs;

public final class GripsStartPositionsProtos {
  private GripsStartPositionsProtos() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }
  public interface GripsStartPositionsOrBuilder extends
      // @@protoc_insertion_point(interface_extends:llsf_msgs.GripsStartPositions)
      com.google.protobuf.MessageOrBuilder {

    /**
     * <code>required float startposition_x = 1;</code>
     * @return Whether the startpositionX field is set.
     */
    boolean hasStartpositionX();
    /**
     * <code>required float startposition_x = 1;</code>
     * @return The startpositionX.
     */
    float getStartpositionX();

    /**
     * <code>required float startposition_y = 2;</code>
     * @return Whether the startpositionY field is set.
     */
    boolean hasStartpositionY();
    /**
     * <code>required float startposition_y = 2;</code>
     * @return The startpositionY.
     */
    float getStartpositionY();

    /**
     * <code>required float startposition_yaw = 3;</code>
     * @return Whether the startpositionYaw field is set.
     */
    boolean hasStartpositionYaw();
    /**
     * <code>required float startposition_yaw = 3;</code>
     * @return The startpositionYaw.
     */
    float getStartpositionYaw();
  }
  /**
   * <pre>
   * Set the starting positions of the robots
   * </pre>
   *
   * Protobuf type {@code llsf_msgs.GripsStartPositions}
   */
  public  static final class GripsStartPositions extends
      com.google.protobuf.GeneratedMessageV3 implements
      // @@protoc_insertion_point(message_implements:llsf_msgs.GripsStartPositions)
      GripsStartPositionsOrBuilder {
  private static final long serialVersionUID = 0L;
    // Use GripsStartPositions.newBuilder() to construct.
    private GripsStartPositions(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
      super(builder);
    }
    private GripsStartPositions() {
    }

    @java.lang.Override
    @SuppressWarnings({"unused"})
    protected java.lang.Object newInstance(
        UnusedPrivateParameter unused) {
      return new GripsStartPositions();
    }

    @java.lang.Override
    public final com.google.protobuf.UnknownFieldSet
    getUnknownFields() {
      return this.unknownFields;
    }
    private GripsStartPositions(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      this();
      if (extensionRegistry == null) {
        throw new java.lang.NullPointerException();
      }
      int mutable_bitField0_ = 0;
      com.google.protobuf.UnknownFieldSet.Builder unknownFields =
          com.google.protobuf.UnknownFieldSet.newBuilder();
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            case 13: {
              bitField0_ |= 0x00000001;
              startpositionX_ = input.readFloat();
              break;
            }
            case 21: {
              bitField0_ |= 0x00000002;
              startpositionY_ = input.readFloat();
              break;
            }
            case 29: {
              bitField0_ |= 0x00000004;
              startpositionYaw_ = input.readFloat();
              break;
            }
            default: {
              if (!parseUnknownField(
                  input, unknownFields, extensionRegistry, tag)) {
                done = true;
              }
              break;
            }
          }
        }
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(this);
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(
            e).setUnfinishedMessage(this);
      } finally {
        this.unknownFields = unknownFields.build();
        makeExtensionsImmutable();
      }
    }
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return org.robocup_logistics.llsf_msgs.GripsStartPositionsProtos.internal_static_llsf_msgs_GripsStartPositions_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return org.robocup_logistics.llsf_msgs.GripsStartPositionsProtos.internal_static_llsf_msgs_GripsStartPositions_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              org.robocup_logistics.llsf_msgs.GripsStartPositionsProtos.GripsStartPositions.class, org.robocup_logistics.llsf_msgs.GripsStartPositionsProtos.GripsStartPositions.Builder.class);
    }

    /**
     * Protobuf enum {@code llsf_msgs.GripsStartPositions.CompType}
     */
    public enum CompType
        implements com.google.protobuf.ProtocolMessageEnum {
      /**
       * <code>COMP_ID = 5000;</code>
       */
      COMP_ID(5000),
      /**
       * <code>MSG_TYPE = 504;</code>
       */
      MSG_TYPE(504),
      ;

      /**
       * <code>COMP_ID = 5000;</code>
       */
      public static final int COMP_ID_VALUE = 5000;
      /**
       * <code>MSG_TYPE = 504;</code>
       */
      public static final int MSG_TYPE_VALUE = 504;


      public final int getNumber() {
        return value;
      }

      /**
       * @param value The numeric wire value of the corresponding enum entry.
       * @return The enum associated with the given numeric wire value.
       * @deprecated Use {@link #forNumber(int)} instead.
       */
      @java.lang.Deprecated
      public static CompType valueOf(int value) {
        return forNumber(value);
      }

      /**
       * @param value The numeric wire value of the corresponding enum entry.
       * @return The enum associated with the given numeric wire value.
       */
      public static CompType forNumber(int value) {
        switch (value) {
          case 5000: return COMP_ID;
          case 504: return MSG_TYPE;
          default: return null;
        }
      }

      public static com.google.protobuf.Internal.EnumLiteMap<CompType>
          internalGetValueMap() {
        return internalValueMap;
      }
      private static final com.google.protobuf.Internal.EnumLiteMap<
          CompType> internalValueMap =
            new com.google.protobuf.Internal.EnumLiteMap<CompType>() {
              public CompType findValueByNumber(int number) {
                return CompType.forNumber(number);
              }
            };

      public final com.google.protobuf.Descriptors.EnumValueDescriptor
          getValueDescriptor() {
        return getDescriptor().getValues().get(ordinal());
      }
      public final com.google.protobuf.Descriptors.EnumDescriptor
          getDescriptorForType() {
        return getDescriptor();
      }
      public static final com.google.protobuf.Descriptors.EnumDescriptor
          getDescriptor() {
        return org.robocup_logistics.llsf_msgs.GripsStartPositionsProtos.GripsStartPositions.getDescriptor().getEnumTypes().get(0);
      }

      private static final CompType[] VALUES = values();

      public static CompType valueOf(
          com.google.protobuf.Descriptors.EnumValueDescriptor desc) {
        if (desc.getType() != getDescriptor()) {
          throw new java.lang.IllegalArgumentException(
            "EnumValueDescriptor is not for this type.");
        }
        return VALUES[desc.getIndex()];
      }

      private final int value;

      private CompType(int value) {
        this.value = value;
      }

      // @@protoc_insertion_point(enum_scope:llsf_msgs.GripsStartPositions.CompType)
    }

    private int bitField0_;
    public static final int STARTPOSITION_X_FIELD_NUMBER = 1;
    private float startpositionX_;
    /**
     * <code>required float startposition_x = 1;</code>
     * @return Whether the startpositionX field is set.
     */
    public boolean hasStartpositionX() {
      return ((bitField0_ & 0x00000001) != 0);
    }
    /**
     * <code>required float startposition_x = 1;</code>
     * @return The startpositionX.
     */
    public float getStartpositionX() {
      return startpositionX_;
    }

    public static final int STARTPOSITION_Y_FIELD_NUMBER = 2;
    private float startpositionY_;
    /**
     * <code>required float startposition_y = 2;</code>
     * @return Whether the startpositionY field is set.
     */
    public boolean hasStartpositionY() {
      return ((bitField0_ & 0x00000002) != 0);
    }
    /**
     * <code>required float startposition_y = 2;</code>
     * @return The startpositionY.
     */
    public float getStartpositionY() {
      return startpositionY_;
    }

    public static final int STARTPOSITION_YAW_FIELD_NUMBER = 3;
    private float startpositionYaw_;
    /**
     * <code>required float startposition_yaw = 3;</code>
     * @return Whether the startpositionYaw field is set.
     */
    public boolean hasStartpositionYaw() {
      return ((bitField0_ & 0x00000004) != 0);
    }
    /**
     * <code>required float startposition_yaw = 3;</code>
     * @return The startpositionYaw.
     */
    public float getStartpositionYaw() {
      return startpositionYaw_;
    }

    private byte memoizedIsInitialized = -1;
    @java.lang.Override
    public final boolean isInitialized() {
      byte isInitialized = memoizedIsInitialized;
      if (isInitialized == 1) return true;
      if (isInitialized == 0) return false;

      if (!hasStartpositionX()) {
        memoizedIsInitialized = 0;
        return false;
      }
      if (!hasStartpositionY()) {
        memoizedIsInitialized = 0;
        return false;
      }
      if (!hasStartpositionYaw()) {
        memoizedIsInitialized = 0;
        return false;
      }
      memoizedIsInitialized = 1;
      return true;
    }

    @java.lang.Override
    public void writeTo(com.google.protobuf.CodedOutputStream output)
                        throws java.io.IOException {
      if (((bitField0_ & 0x00000001) != 0)) {
        output.writeFloat(1, startpositionX_);
      }
      if (((bitField0_ & 0x00000002) != 0)) {
        output.writeFloat(2, startpositionY_);
      }
      if (((bitField0_ & 0x00000004) != 0)) {
        output.writeFloat(3, startpositionYaw_);
      }
      unknownFields.writeTo(output);
    }

    @java.lang.Override
    public int getSerializedSize() {
      int size = memoizedSize;
      if (size != -1) return size;

      size = 0;
      if (((bitField0_ & 0x00000001) != 0)) {
        size += com.google.protobuf.CodedOutputStream
          .computeFloatSize(1, startpositionX_);
      }
      if (((bitField0_ & 0x00000002) != 0)) {
        size += com.google.protobuf.CodedOutputStream
          .computeFloatSize(2, startpositionY_);
      }
      if (((bitField0_ & 0x00000004) != 0)) {
        size += com.google.protobuf.CodedOutputStream
          .computeFloatSize(3, startpositionYaw_);
      }
      size += unknownFields.getSerializedSize();
      memoizedSize = size;
      return size;
    }

    @java.lang.Override
    public boolean equals(final java.lang.Object obj) {
      if (obj == this) {
       return true;
      }
      if (!(obj instanceof org.robocup_logistics.llsf_msgs.GripsStartPositionsProtos.GripsStartPositions)) {
        return super.equals(obj);
      }
      org.robocup_logistics.llsf_msgs.GripsStartPositionsProtos.GripsStartPositions other = (org.robocup_logistics.llsf_msgs.GripsStartPositionsProtos.GripsStartPositions) obj;

      if (hasStartpositionX() != other.hasStartpositionX()) return false;
      if (hasStartpositionX()) {
        if (java.lang.Float.floatToIntBits(getStartpositionX())
            != java.lang.Float.floatToIntBits(
                other.getStartpositionX())) return false;
      }
      if (hasStartpositionY() != other.hasStartpositionY()) return false;
      if (hasStartpositionY()) {
        if (java.lang.Float.floatToIntBits(getStartpositionY())
            != java.lang.Float.floatToIntBits(
                other.getStartpositionY())) return false;
      }
      if (hasStartpositionYaw() != other.hasStartpositionYaw()) return false;
      if (hasStartpositionYaw()) {
        if (java.lang.Float.floatToIntBits(getStartpositionYaw())
            != java.lang.Float.floatToIntBits(
                other.getStartpositionYaw())) return false;
      }
      if (!unknownFields.equals(other.unknownFields)) return false;
      return true;
    }

    @java.lang.Override
    public int hashCode() {
      if (memoizedHashCode != 0) {
        return memoizedHashCode;
      }
      int hash = 41;
      hash = (19 * hash) + getDescriptor().hashCode();
      if (hasStartpositionX()) {
        hash = (37 * hash) + STARTPOSITION_X_FIELD_NUMBER;
        hash = (53 * hash) + java.lang.Float.floatToIntBits(
            getStartpositionX());
      }
      if (hasStartpositionY()) {
        hash = (37 * hash) + STARTPOSITION_Y_FIELD_NUMBER;
        hash = (53 * hash) + java.lang.Float.floatToIntBits(
            getStartpositionY());
      }
      if (hasStartpositionYaw()) {
        hash = (37 * hash) + STARTPOSITION_YAW_FIELD_NUMBER;
        hash = (53 * hash) + java.lang.Float.floatToIntBits(
            getStartpositionYaw());
      }
      hash = (29 * hash) + unknownFields.hashCode();
      memoizedHashCode = hash;
      return hash;
    }

    public static org.robocup_logistics.llsf_msgs.GripsStartPositionsProtos.GripsStartPositions parseFrom(
        java.nio.ByteBuffer data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static org.robocup_logistics.llsf_msgs.GripsStartPositionsProtos.GripsStartPositions parseFrom(
        java.nio.ByteBuffer data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static org.robocup_logistics.llsf_msgs.GripsStartPositionsProtos.GripsStartPositions parseFrom(
        com.google.protobuf.ByteString data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static org.robocup_logistics.llsf_msgs.GripsStartPositionsProtos.GripsStartPositions parseFrom(
        com.google.protobuf.ByteString data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static org.robocup_logistics.llsf_msgs.GripsStartPositionsProtos.GripsStartPositions parseFrom(byte[] data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static org.robocup_logistics.llsf_msgs.GripsStartPositionsProtos.GripsStartPositions parseFrom(
        byte[] data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static org.robocup_logistics.llsf_msgs.GripsStartPositionsProtos.GripsStartPositions parseFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input);
    }
    public static org.robocup_logistics.llsf_msgs.GripsStartPositionsProtos.GripsStartPositions parseFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input, extensionRegistry);
    }
    public static org.robocup_logistics.llsf_msgs.GripsStartPositionsProtos.GripsStartPositions parseDelimitedFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseDelimitedWithIOException(PARSER, input);
    }
    public static org.robocup_logistics.llsf_msgs.GripsStartPositionsProtos.GripsStartPositions parseDelimitedFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
    }
    public static org.robocup_logistics.llsf_msgs.GripsStartPositionsProtos.GripsStartPositions parseFrom(
        com.google.protobuf.CodedInputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input);
    }
    public static org.robocup_logistics.llsf_msgs.GripsStartPositionsProtos.GripsStartPositions parseFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input, extensionRegistry);
    }

    @java.lang.Override
    public Builder newBuilderForType() { return newBuilder(); }
    public static Builder newBuilder() {
      return DEFAULT_INSTANCE.toBuilder();
    }
    public static Builder newBuilder(org.robocup_logistics.llsf_msgs.GripsStartPositionsProtos.GripsStartPositions prototype) {
      return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
    }
    @java.lang.Override
    public Builder toBuilder() {
      return this == DEFAULT_INSTANCE
          ? new Builder() : new Builder().mergeFrom(this);
    }

    @java.lang.Override
    protected Builder newBuilderForType(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      Builder builder = new Builder(parent);
      return builder;
    }
    /**
     * <pre>
     * Set the starting positions of the robots
     * </pre>
     *
     * Protobuf type {@code llsf_msgs.GripsStartPositions}
     */
    public static final class Builder extends
        com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
        // @@protoc_insertion_point(builder_implements:llsf_msgs.GripsStartPositions)
        org.robocup_logistics.llsf_msgs.GripsStartPositionsProtos.GripsStartPositionsOrBuilder {
      public static final com.google.protobuf.Descriptors.Descriptor
          getDescriptor() {
        return org.robocup_logistics.llsf_msgs.GripsStartPositionsProtos.internal_static_llsf_msgs_GripsStartPositions_descriptor;
      }

      @java.lang.Override
      protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
          internalGetFieldAccessorTable() {
        return org.robocup_logistics.llsf_msgs.GripsStartPositionsProtos.internal_static_llsf_msgs_GripsStartPositions_fieldAccessorTable
            .ensureFieldAccessorsInitialized(
                org.robocup_logistics.llsf_msgs.GripsStartPositionsProtos.GripsStartPositions.class, org.robocup_logistics.llsf_msgs.GripsStartPositionsProtos.GripsStartPositions.Builder.class);
      }

      // Construct using org.robocup_logistics.llsf_msgs.GripsStartPositionsProtos.GripsStartPositions.newBuilder()
      private Builder() {
        maybeForceBuilderInitialization();
      }

      private Builder(
          com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
        super(parent);
        maybeForceBuilderInitialization();
      }
      private void maybeForceBuilderInitialization() {
        if (com.google.protobuf.GeneratedMessageV3
                .alwaysUseFieldBuilders) {
        }
      }
      @java.lang.Override
      public Builder clear() {
        super.clear();
        startpositionX_ = 0F;
        bitField0_ = (bitField0_ & ~0x00000001);
        startpositionY_ = 0F;
        bitField0_ = (bitField0_ & ~0x00000002);
        startpositionYaw_ = 0F;
        bitField0_ = (bitField0_ & ~0x00000004);
        return this;
      }

      @java.lang.Override
      public com.google.protobuf.Descriptors.Descriptor
          getDescriptorForType() {
        return org.robocup_logistics.llsf_msgs.GripsStartPositionsProtos.internal_static_llsf_msgs_GripsStartPositions_descriptor;
      }

      @java.lang.Override
      public org.robocup_logistics.llsf_msgs.GripsStartPositionsProtos.GripsStartPositions getDefaultInstanceForType() {
        return org.robocup_logistics.llsf_msgs.GripsStartPositionsProtos.GripsStartPositions.getDefaultInstance();
      }

      @java.lang.Override
      public org.robocup_logistics.llsf_msgs.GripsStartPositionsProtos.GripsStartPositions build() {
        org.robocup_logistics.llsf_msgs.GripsStartPositionsProtos.GripsStartPositions result = buildPartial();
        if (!result.isInitialized()) {
          throw newUninitializedMessageException(result);
        }
        return result;
      }

      @java.lang.Override
      public org.robocup_logistics.llsf_msgs.GripsStartPositionsProtos.GripsStartPositions buildPartial() {
        org.robocup_logistics.llsf_msgs.GripsStartPositionsProtos.GripsStartPositions result = new org.robocup_logistics.llsf_msgs.GripsStartPositionsProtos.GripsStartPositions(this);
        int from_bitField0_ = bitField0_;
        int to_bitField0_ = 0;
        if (((from_bitField0_ & 0x00000001) != 0)) {
          result.startpositionX_ = startpositionX_;
          to_bitField0_ |= 0x00000001;
        }
        if (((from_bitField0_ & 0x00000002) != 0)) {
          result.startpositionY_ = startpositionY_;
          to_bitField0_ |= 0x00000002;
        }
        if (((from_bitField0_ & 0x00000004) != 0)) {
          result.startpositionYaw_ = startpositionYaw_;
          to_bitField0_ |= 0x00000004;
        }
        result.bitField0_ = to_bitField0_;
        onBuilt();
        return result;
      }

      @java.lang.Override
      public Builder clone() {
        return super.clone();
      }
      @java.lang.Override
      public Builder setField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          java.lang.Object value) {
        return super.setField(field, value);
      }
      @java.lang.Override
      public Builder clearField(
          com.google.protobuf.Descriptors.FieldDescriptor field) {
        return super.clearField(field);
      }
      @java.lang.Override
      public Builder clearOneof(
          com.google.protobuf.Descriptors.OneofDescriptor oneof) {
        return super.clearOneof(oneof);
      }
      @java.lang.Override
      public Builder setRepeatedField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          int index, java.lang.Object value) {
        return super.setRepeatedField(field, index, value);
      }
      @java.lang.Override
      public Builder addRepeatedField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          java.lang.Object value) {
        return super.addRepeatedField(field, value);
      }
      @java.lang.Override
      public Builder mergeFrom(com.google.protobuf.Message other) {
        if (other instanceof org.robocup_logistics.llsf_msgs.GripsStartPositionsProtos.GripsStartPositions) {
          return mergeFrom((org.robocup_logistics.llsf_msgs.GripsStartPositionsProtos.GripsStartPositions)other);
        } else {
          super.mergeFrom(other);
          return this;
        }
      }

      public Builder mergeFrom(org.robocup_logistics.llsf_msgs.GripsStartPositionsProtos.GripsStartPositions other) {
        if (other == org.robocup_logistics.llsf_msgs.GripsStartPositionsProtos.GripsStartPositions.getDefaultInstance()) return this;
        if (other.hasStartpositionX()) {
          setStartpositionX(other.getStartpositionX());
        }
        if (other.hasStartpositionY()) {
          setStartpositionY(other.getStartpositionY());
        }
        if (other.hasStartpositionYaw()) {
          setStartpositionYaw(other.getStartpositionYaw());
        }
        this.mergeUnknownFields(other.unknownFields);
        onChanged();
        return this;
      }

      @java.lang.Override
      public final boolean isInitialized() {
        if (!hasStartpositionX()) {
          return false;
        }
        if (!hasStartpositionY()) {
          return false;
        }
        if (!hasStartpositionYaw()) {
          return false;
        }
        return true;
      }

      @java.lang.Override
      public Builder mergeFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws java.io.IOException {
        org.robocup_logistics.llsf_msgs.GripsStartPositionsProtos.GripsStartPositions parsedMessage = null;
        try {
          parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
        } catch (com.google.protobuf.InvalidProtocolBufferException e) {
          parsedMessage = (org.robocup_logistics.llsf_msgs.GripsStartPositionsProtos.GripsStartPositions) e.getUnfinishedMessage();
          throw e.unwrapIOException();
        } finally {
          if (parsedMessage != null) {
            mergeFrom(parsedMessage);
          }
        }
        return this;
      }
      private int bitField0_;

      private float startpositionX_ ;
      /**
       * <code>required float startposition_x = 1;</code>
       * @return Whether the startpositionX field is set.
       */
      public boolean hasStartpositionX() {
        return ((bitField0_ & 0x00000001) != 0);
      }
      /**
       * <code>required float startposition_x = 1;</code>
       * @return The startpositionX.
       */
      public float getStartpositionX() {
        return startpositionX_;
      }
      /**
       * <code>required float startposition_x = 1;</code>
       * @param value The startpositionX to set.
       * @return This builder for chaining.
       */
      public Builder setStartpositionX(float value) {
        bitField0_ |= 0x00000001;
        startpositionX_ = value;
        onChanged();
        return this;
      }
      /**
       * <code>required float startposition_x = 1;</code>
       * @return This builder for chaining.
       */
      public Builder clearStartpositionX() {
        bitField0_ = (bitField0_ & ~0x00000001);
        startpositionX_ = 0F;
        onChanged();
        return this;
      }

      private float startpositionY_ ;
      /**
       * <code>required float startposition_y = 2;</code>
       * @return Whether the startpositionY field is set.
       */
      public boolean hasStartpositionY() {
        return ((bitField0_ & 0x00000002) != 0);
      }
      /**
       * <code>required float startposition_y = 2;</code>
       * @return The startpositionY.
       */
      public float getStartpositionY() {
        return startpositionY_;
      }
      /**
       * <code>required float startposition_y = 2;</code>
       * @param value The startpositionY to set.
       * @return This builder for chaining.
       */
      public Builder setStartpositionY(float value) {
        bitField0_ |= 0x00000002;
        startpositionY_ = value;
        onChanged();
        return this;
      }
      /**
       * <code>required float startposition_y = 2;</code>
       * @return This builder for chaining.
       */
      public Builder clearStartpositionY() {
        bitField0_ = (bitField0_ & ~0x00000002);
        startpositionY_ = 0F;
        onChanged();
        return this;
      }

      private float startpositionYaw_ ;
      /**
       * <code>required float startposition_yaw = 3;</code>
       * @return Whether the startpositionYaw field is set.
       */
      public boolean hasStartpositionYaw() {
        return ((bitField0_ & 0x00000004) != 0);
      }
      /**
       * <code>required float startposition_yaw = 3;</code>
       * @return The startpositionYaw.
       */
      public float getStartpositionYaw() {
        return startpositionYaw_;
      }
      /**
       * <code>required float startposition_yaw = 3;</code>
       * @param value The startpositionYaw to set.
       * @return This builder for chaining.
       */
      public Builder setStartpositionYaw(float value) {
        bitField0_ |= 0x00000004;
        startpositionYaw_ = value;
        onChanged();
        return this;
      }
      /**
       * <code>required float startposition_yaw = 3;</code>
       * @return This builder for chaining.
       */
      public Builder clearStartpositionYaw() {
        bitField0_ = (bitField0_ & ~0x00000004);
        startpositionYaw_ = 0F;
        onChanged();
        return this;
      }
      @java.lang.Override
      public final Builder setUnknownFields(
          final com.google.protobuf.UnknownFieldSet unknownFields) {
        return super.setUnknownFields(unknownFields);
      }

      @java.lang.Override
      public final Builder mergeUnknownFields(
          final com.google.protobuf.UnknownFieldSet unknownFields) {
        return super.mergeUnknownFields(unknownFields);
      }


      // @@protoc_insertion_point(builder_scope:llsf_msgs.GripsStartPositions)
    }

    // @@protoc_insertion_point(class_scope:llsf_msgs.GripsStartPositions)
    private static final org.robocup_logistics.llsf_msgs.GripsStartPositionsProtos.GripsStartPositions DEFAULT_INSTANCE;
    static {
      DEFAULT_INSTANCE = new org.robocup_logistics.llsf_msgs.GripsStartPositionsProtos.GripsStartPositions();
    }

    public static org.robocup_logistics.llsf_msgs.GripsStartPositionsProtos.GripsStartPositions getDefaultInstance() {
      return DEFAULT_INSTANCE;
    }

    @java.lang.Deprecated public static final com.google.protobuf.Parser<GripsStartPositions>
        PARSER = new com.google.protobuf.AbstractParser<GripsStartPositions>() {
      @java.lang.Override
      public GripsStartPositions parsePartialFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws com.google.protobuf.InvalidProtocolBufferException {
        return new GripsStartPositions(input, extensionRegistry);
      }
    };

    public static com.google.protobuf.Parser<GripsStartPositions> parser() {
      return PARSER;
    }

    @java.lang.Override
    public com.google.protobuf.Parser<GripsStartPositions> getParserForType() {
      return PARSER;
    }

    @java.lang.Override
    public org.robocup_logistics.llsf_msgs.GripsStartPositionsProtos.GripsStartPositions getDefaultInstanceForType() {
      return DEFAULT_INSTANCE;
    }

  }

  private static final com.google.protobuf.Descriptors.Descriptor
    internal_static_llsf_msgs_GripsStartPositions_descriptor;
  private static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_llsf_msgs_GripsStartPositions_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\031GripsStartPositions.proto\022\tllsf_msgs\"\213" +
      "\001\n\023GripsStartPositions\022\027\n\017startposition_" +
      "x\030\001 \002(\002\022\027\n\017startposition_y\030\002 \002(\002\022\031\n\021star" +
      "tposition_yaw\030\003 \002(\002\"\'\n\010CompType\022\014\n\007COMP_" +
      "ID\020\210\'\022\r\n\010MSG_TYPE\020\370\003B<\n\037org.robocup_logi" +
      "stics.llsf_msgsB\031GripsStartPositionsProt" +
      "os"
    };
    descriptor = com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
        });
    internal_static_llsf_msgs_GripsStartPositions_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_llsf_msgs_GripsStartPositions_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_llsf_msgs_GripsStartPositions_descriptor,
        new java.lang.String[] { "StartpositionX", "StartpositionY", "StartpositionYaw", });
  }

  // @@protoc_insertion_point(outer_class_scope)
}
