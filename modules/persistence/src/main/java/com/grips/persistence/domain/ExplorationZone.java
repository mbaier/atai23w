/*
 *
 * Copyright (c) 2017, Graz Robust and Intelligent Production System (grips)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.grips.persistence.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.rcll.domain.ZoneName;
import com.shared.domain.Point2d;
import com.shared.domain.ZoneExplorationState;
import com.visualization.domain.VisualizationExplorationZone;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.apachecommons.CommonsLog;

import java.util.*;

@Getter
@Setter
@CommonsLog
public class ExplorationZone extends Zone implements VisualizationExplorationZone {
    protected Point2d zoneCenter;

    protected String zonePrefix;

    protected int heightIDX;

    protected int widthIDX;

    private long zoneNumber;

    protected ArrayList<ExplorationObservation> observations;

    protected ZoneExplorationState explorationState;

    @JsonIgnore
    protected ExplorationZone mirroredZone;

    protected ZoneState zoneState;

    private String machine;

    private ZoneOccupiedState zoneOccupiedState;

    public String getMachine() {
        return machine;
    }

    public void setMachine(String machine) {
        this.machine = machine;
    }

    public ZoneOccupiedState getZoneOccupiedState() {
        return zoneOccupiedState;
    }

    public void setZoneOccupiedState(ZoneOccupiedState zoneOccupiedState) {
        this.zoneOccupiedState = zoneOccupiedState;
    }

    public enum ZoneOccupiedState {MACHINE, BLOCKED, UNKNOWN, FREE}

    public ExplorationZone(String zonePrefix, int zoneHeightIdx, int zoneWidthIdx, int fieldWidth, int fieldHeight, List<Integer> insertionZones) {
        if (zoneHeightIdx > fieldHeight) {
            log.error("Zone height out of bounds!");
        }
        if (zoneWidthIdx > (fieldWidth)) {
            log.error("Zone width out of bounds!");
        }

        this.zonePrefix = zonePrefix;
        this.zoneNumber = zoneHeightIdx + 10 * zoneWidthIdx;
        this.heightIDX = zoneHeightIdx;
        this.widthIDX = zoneWidthIdx;
        this.zoneName = zonePrefix + "_Z" + this.zoneNumber;
        this.observations = new ArrayList<>();
        if (insertionZones.contains(new Long(zoneNumber).intValue())) {
            this.setZoneState(ExplorationZone.ZoneState.INSERTION);
            this.setExplorationState(ZoneExplorationState.EXPLORED);
        } else {
            this.setZoneState(ExplorationZone.ZoneState.VALID);
            this.setExplorationState(ZoneExplorationState.UNEXPLORED);
        }
        double yPos = heightIDX - 0.5;
        double xPos = widthIDX - 0.5;
        if (0 == zonePrefix.compareToIgnoreCase("M")) {
            xPos *= -1;
        }
        this.zoneCenter = new Point2d(xPos, yPos);
    }

    public enum ZoneState {INSERTION, FREE, VALID}

    public ZoneName getZoneNameSdk() {
        return new ZoneName(this.zoneName);
    }
}
