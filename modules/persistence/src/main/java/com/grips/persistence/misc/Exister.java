package com.grips.persistence.misc;

public interface Exister<ID> {
    boolean existsById(ID id);
}
