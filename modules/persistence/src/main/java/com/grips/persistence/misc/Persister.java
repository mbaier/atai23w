package com.grips.persistence.misc;

public interface Persister<T> {
    <S extends T> S save(S entity);
}
