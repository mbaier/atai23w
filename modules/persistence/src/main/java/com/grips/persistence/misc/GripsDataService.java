package com.grips.persistence.misc;

public interface GripsDataService<T, ID> extends Fetcher<T, ID>, Persister<T>, Exister<ID> {
}
