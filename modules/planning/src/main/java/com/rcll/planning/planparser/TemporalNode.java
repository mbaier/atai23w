package com.rcll.planning.planparser;

import java.util.HashMap;

public class TemporalNode {

    //todo replace with enum!
    private int type; //1->action start 2->action end 3->plan start
    private String name;
    private int id;
    private int [] instantiation;
    private double time;


    private double relaxedTime;
    private boolean valid; //if false, the node has been invalidated due to an order deleting

    private double duration;

    private int specialInput; //0->nothing, 1->shelf, 2->slide
    private final HashMap<Integer, Double> softDeadline;
    private final HashMap<Integer, Double> hardDeadline;


    public TemporalNode(int type, String name, int id, double time, double duration) {
        this.type = type;
        this.name = name;
        this.id = id;
        this.time = time;
        this.valid = true;
        this.relaxedTime = -1;
        this.duration = duration;
        softDeadline = new HashMap<>();
        hardDeadline = new HashMap<>();
    }

    public TemporalNode(int type, String name, int id, double time) {
        this.type = type;
        this.name = name;
        this.id = id;
        this.time = time;
        this.valid = true;
        this.relaxedTime = -1;
        softDeadline = new HashMap<>();
        hardDeadline = new HashMap<>();
    }

    public int getSpecialInput() {
        return specialInput;
    }

    public void setSpecialInput(int specialInput) {
        this.specialInput = specialInput;
    }

    public void addDeadline (int key, double dl) {
        softDeadline.put(key,dl);
    }

    public void addHardDeadline (int key, double dl) {
        hardDeadline.put(key,dl);
    }

    public HashMap<Integer, Double> getSoftDeadline() {
        return softDeadline;
    }

    public HashMap<Integer, Double> getHardDeadline() {
        return hardDeadline;
    }

    public double getRelaxedTime() {
        return relaxedTime;
    }

    public void setRelaxedTime(double relaxedTime) {
        this.relaxedTime = relaxedTime;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public int[] getInstantiation() {
        return instantiation;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String toString () {
        return name;
    }

    public void setInstantiation(int[] instantiation) {
        this.instantiation = instantiation;
    }

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = time;
    }

    public String prettyPrint() {
        String s = "";
        if (specialInput == 1) {
            s = relaxedTime + ": (" + name.split("_start")[0].replace(',',' ').replace('(',' ') + " [" + duration + "]";
            String temp = s.split("\\) " )[0];
            String [] parameters = temp.split(" ");
            String lastParameter = parameters[parameters.length-1];
            s = s.replace(lastParameter,lastParameter.replace("input","shelf"));
        } else if (specialInput == 2) {
            s = relaxedTime + ": (" + name.split("_start")[0].replace(',',' ').replace('(',' ') + " [" + duration + "]";
            String temp = s.split("\\) " )[0];
            String [] parameters = temp.split(" ");
            String lastParameter = parameters[parameters.length-1];
            s = s.replace(lastParameter,lastParameter.replace("input","slide"));
        } else
            s = relaxedTime + ": (" + name.split("_start")[0].replace(',',' ').replace('(',' ') + " [" + duration + "]";
        return s;
    }

    public String print () {
        return relaxedTime + ": " + name;
    }


}
