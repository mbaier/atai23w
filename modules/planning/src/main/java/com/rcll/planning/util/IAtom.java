package com.rcll.planning.util;

public interface IAtom {
    boolean isPredicate();
    String getName();
    double getResult();
    String getAttributes();
}
