package com.rcll.planning.planparser;

import com.rcll.domain.Order;
import com.rcll.domain.ProductComplexity;
import com.rcll.planning.encoding.RcllCodedProblem;
import com.rcll.planning.encoding.IntOp;
import com.rcll.planning.util.RcllTemporalPlan;
import com.rcll.refbox.RefboxClient;
import fr.uga.pddl4j.util.IntExp;
import lombok.extern.apachecommons.CommonsLog;
import org.apache.commons.collections4.iterators.ReverseListIterator;
import org.jgrapht.graph.SimpleDirectedGraph;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
@CommonsLog
public class TemporalGraph {

    private final RcllCodedProblem cp;
    private final RcllTemporalPlan plan;
    private int graphSize;
    private final TreeMap<Double, Set<ExtIntOp>> orderedActions;
    private final SimpleDirectedGraph<TemporalNode, TemporalEdge> tgraph;
    private TemporalNode firstR2Node;
    private TemporalNode firstR3Node;
    private TemporalNode fakeNodeR2;
    private TemporalNode fakeNodeR3;

    private final double minValue = 0.001;


    private final RefboxClient refboxClient;

    public TemporalGraph(RcllCodedProblem cp, RcllTemporalPlan plan, RefboxClient refboxClient) {
        this.cp = cp;
        this.plan = plan;
        this.refboxClient = refboxClient;
        tgraph = new SimpleDirectedGraph<>(TemporalEdge.class);
        orderedActions = new TreeMap<Double, Set<ExtIntOp>>();
        graphSize = 0;
    }

    public TemporalGraph(RcllCodedProblem cp, RcllTemporalPlan plan) {
        this.cp = cp;
        this.plan = plan;
        tgraph = new SimpleDirectedGraph<>(TemporalEdge.class);
        orderedActions = new TreeMap<Double, Set<ExtIntOp>>();
        this.refboxClient = null;
        graphSize = 0;
    }

    public TemporalNode getFirstR2Node() {
        return firstR2Node;
    }

    public TemporalNode getFirstR3Node() {
        return firstR3Node;
    }

    public TemporalNode getFakeNodeR2() {
        return fakeNodeR2;
    }

    public TemporalNode getFakeNodeR3() {
        return fakeNodeR3;
    }

    public void createTemporalGraph () {
        TemporalNode start = new TemporalNode(3, "plan_start", graphSize, 0);
        addNode(start);
        List<String> constants = cp.getConstants();

        //adding all the nodes to the graph + the start-end edges
        TreeMap<Double, Set<IntOp>> actions = plan.actions();

        ArrayList<Integer> orderDelivery = new ArrayList<>();

        for (Map.Entry<Double, Set<IntOp>> entry : actions.entrySet()) {
            Set<IntOp> seti = entry.getValue();
            Iterator<IntOp> setIt = seti.iterator();
            while(setIt.hasNext()){
                IntOp i = setIt.next();
                int [] instantiations = i.getInstantiations();
                String nodeName = i.getName() + "(" + constants.get(instantiations[0]);
                for(int j=1; j < instantiations.length; j++)
                    nodeName = nodeName + "," + constants.get(instantiations[j]);
                nodeName = nodeName + ")";
                TemporalNode actionStart = new TemporalNode(1 , nodeName + "_start", graphSize, entry.getKey(), i.getDuration());
                actionStart.setInstantiation(i.getInstantiations());
                actionStart.setSpecialInput(i.getSpecialInput());
                addNode(actionStart);
                TemporalNode actionEnd = new TemporalNode(2 , nodeName + "_end", graphSize, entry.getKey() + i.getDuration());
                actionEnd.setInstantiation(i.getInstantiations());
                addNode(actionEnd);
                tgraph.addEdge(actionStart,actionEnd, new TemporalEdge(i.getDuration(),i.getDuration(),2) );

                if (i.getName().equals("deliverproducttodscriticaltask"))
                    orderDelivery.add(graphSize-1);

                Set<ExtIntOp> buff = orderedActions.get(entry.getKey());
                if (buff == null) {
                    buff = new HashSet<ExtIntOp>();
                    this.orderedActions.put(entry.getKey(), buff);
                }
                buff.add(new ExtIntOp(i,1, graphSize - 2));

                buff = orderedActions.get(entry.getKey() + i.getDuration());
                if (buff == null) {
                    buff = new HashSet<ExtIntOp>();
                    this.orderedActions.put(entry.getKey() + i.getDuration(), buff);
                }
                buff.add(new ExtIntOp(i,2, graphSize - 1));



            }
        }

        addEdges(start);


        if (refboxClient != null) {         //this operation is not needed (and will result in an error) if we are building the temp graph only to refine the macros
            //add softdeadlines
            generateOrderDeadlines(orderDelivery);
            log.info("generating deadlines");
            //add hardDeadLines (end of game)
            generateOrderHardDeadlines(orderDelivery);
        }

        /*use the above code to create a string representing the graph in the DOT format
        DOTExporter<TemporalNode, TemporalEdge> exporter =
                new DOTExporter<>(v -> v.getName().replace('(', '_').replace(')', '_').replace(',', '_') + "_id_" + v.getId());
        exporter.setVertexAttributeProvider((v) -> {
            Map<String, Attribute> map = new LinkedHashMap<>();
            map.put("label", DefaultAttribute.createAttribute(v.toString()));
            return map;
        });
        Writer writer = new StringWriter();
        exporter.exportGraph(tgraph, writer);
        System.out.println(writer);
        */


    }

    //I create a fake node to block the execution of robot 2 and 3 until they exit from the starting zone
    public void addInitialTasks () {
        fakeNodeR2 = new TemporalNode(-1,"fakeNodeR2", -1, -1);
        fakeNodeR3 = new TemporalNode(-1,"fakeNodeR3", -2, -2);

        //System.out.println("++++++++++++++++FIRSTR2NODE+++++++++" +  firstR2Node.toString());
        //System.out.println("++++++++++++++++FIRSTR3NODE+++++++++" +  firstR3Node.toString());

        addNode(fakeNodeR2);
        addNode(fakeNodeR3);

        if (fakeNodeR2 != null && firstR2Node != null)
            tgraph.addEdge(fakeNodeR2, firstR2Node, new TemporalEdge(minValue, Double.MAX_VALUE, 1));
        if (fakeNodeR3 != null && firstR3Node != null)
            tgraph.addEdge(fakeNodeR3, firstR3Node, new TemporalEdge(minValue, Double.MAX_VALUE, 1));
    }

    public Double returnMakespawn () {
        return plan.makespan();
    }


    public void generateOrderDeadlines(ArrayList<Integer> orderDelivery) {
        for (Integer id : orderDelivery) {
            TemporalNode node = getNode(id);
            int orderIdIndex = node.getInstantiation()[2];
            int orderId = Integer.parseInt(cp.getConstants().get(orderIdIndex).substring(1));
            Order order = refboxClient.getOrderById(orderId);
            double deadline = order.getDeliveryPeriodEnd();
            node.addDeadline(orderId, deadline);
            Set<TemporalEdge> inEdges = getInputEdges(node);
            for (TemporalEdge e : inEdges) {
                backPropagateDeadlines(getSource(e), orderId, deadline - e.getLb() );
            }
        }
    }

    public void generateOrderHardDeadlines(ArrayList<Integer> orderDelivery) {
        for (Integer id : orderDelivery) {
            TemporalNode node = getNode(id);
            int orderIdIndex = node.getInstantiation()[2];
            int orderId = Integer.parseInt(cp.getConstants().get(orderIdIndex).substring(1));
            //todo connect with:
            //    @Value("${gameconfig.planningparameters.productiontime}")
            //    private int productiontime;
            double deadline = 1200;
            node.addHardDeadline(orderId, deadline);
            Set<TemporalEdge> inEdges = getInputEdges(node);
            for (TemporalEdge e : inEdges) {
                backPropagateHardDeadlines(getSource(e), orderId, deadline - e.getLb() );
            }
        }
    }



    public void backPropagateDeadlines(TemporalNode node, int orderId, double deadline) {
            HashMap<Integer,Double> nodeDl = node.getSoftDeadline();
            if (nodeDl.containsKey(orderId)) {
                if (nodeDl.get(orderId) > deadline) { //found stricter deadline for the same order, overwrite it
                    node.addDeadline(orderId, deadline);
                    Set<TemporalEdge> inEdges = getInputEdges(node);
                    for (TemporalEdge e : inEdges) {
                        backPropagateDeadlines(getSource(e), orderId, deadline - e.getLb());
                    }
                }
            } else {
                node.addDeadline(orderId, deadline);
                Set<TemporalEdge> inEdges = getInputEdges(node);
                for (TemporalEdge e : inEdges) {
                    backPropagateDeadlines(getSource(e), orderId, deadline - e.getLb());
                }
            }
    }

    public void backPropagateHardDeadlines(TemporalNode node, int orderId, double deadline) {
        HashMap<Integer,Double> nodeDl = node.getHardDeadline();
        if (nodeDl.containsKey(orderId)) {
            if (nodeDl.get(orderId) > deadline) { //found stricter deadline for the same order, overwrite it
                node.addHardDeadline(orderId, deadline);
                Set<TemporalEdge> inEdges = getInputEdges(node);
                for (TemporalEdge e : inEdges) {
                    backPropagateHardDeadlines(getSource(e), orderId, deadline - e.getLb());
                }
            }
        } else {
            node.addHardDeadline(orderId, deadline);
            Set<TemporalEdge> inEdges = getInputEdges(node);
            for (TemporalEdge e : inEdges) {
                backPropagateHardDeadlines(getSource(e), orderId, deadline - e.getLb());
            }
        }
    }

    public boolean checkOrderDeadlines(TemporalNode node, double currentTime) {
        HashMap<Integer, Double> nodeDl = node.getSoftDeadline();
        boolean violatedOrder = false;
        for (int order: nodeDl.keySet()) {
            if (currentTime > nodeDl.get(order)) {
                violatedOrder = true;
                log.warn("Soft deadline " + nodeDl.get(order) + " for order " + order + " will be violated (current time: " + currentTime + " )");
            } else {
                log.info("Soft deadline " + nodeDl.get(order) + " for order " + order + " respected (current time: " + currentTime + " )");
            }
        }
        return violatedOrder;
    }

    public boolean checkOrderHardDeadlines(TemporalNode node, double currentTime) {
        HashMap<Integer, Double> nodeDl = node.getHardDeadline();
        boolean violatedOrder = false;
        for (int order: nodeDl.keySet()) {
            if (currentTime > nodeDl.get(order)) {
                violatedOrder = true;
                log.warn("Hard deadline " + nodeDl.get(order) + " for order " + order + " will be violated (current time: " + currentTime + " )");
            } else {
                log.info("Hard deadline " + nodeDl.get(order) + " for order " + order + " respected (current time: " + currentTime + " )");
            }
        }
        return violatedOrder;
    }

    public void addEdges (TemporalNode start) {
        boolean checkR2 = true;
        boolean checkR3 = true;
        Set<Double> keys = orderedActions.keySet();
        Iterator<Double> itk = keys.iterator();
        while(itk.hasNext()) {
            Double k = itk.next();
            Set<ExtIntOp> ita = orderedActions.get(k);
            Iterator<ExtIntOp> acs = ita.iterator();
            while (acs.hasNext()) {
                ExtIntOp action = acs.next();
                TemporalNode node = getNode(action.getId());

                int [] instantiations = action.getAction().getInstantiations();
                int robotId = Character.getNumericValue(cp.getConstants().get(instantiations[0]).charAt(1));
                if( robotId == 2 && checkR2 ) {
                    firstR2Node = node;
                    checkR2 = false;
                } else if( robotId == 3 && checkR3) {
                    firstR3Node = node;
                    checkR3 = false;
                }

                boolean edge_created = false;
                switch (node.getType()) {

                    case 1:
                        Iterator<IntExp> cit = action.getAction().getPos_pred_atstart().iterator();
                        while (cit.hasNext()) {
                            IntExp prec = cit.next();
                            if (addConditionEdge(node, k, prec, action.getAction().getInstantiations(), false, false)) edge_created = true;
                        }
                        cit = action.getAction().getPos_pred_overall().iterator();
                        while (cit.hasNext()) {
                            IntExp prec = cit.next();
                            if (addConditionEdge(node, k, prec, action.getAction().getInstantiations(), false, true)) edge_created = true;
                        }
                        cit = action.getAction().getNeg_pred_atstart().iterator();
                        while (cit.hasNext()) {
                            IntExp prec = cit.next();
                            if (addConditionEdge(node, k, prec, action.getAction().getInstantiations(), true, false)) edge_created = true;
                        }
                        cit = action.getAction().getNeg_pred_overall().iterator();
                        while (cit.hasNext()) {
                            IntExp prec = cit.next();
                            if (addConditionEdge(node, k, prec, action.getAction().getInstantiations(), true, true)) edge_created = true;
                        }


                        break;

                    case 2:
                        Iterator<IntExp> cite = action.getAction().getPos_pred_atend().iterator();
                        while (cite.hasNext()) {
                            IntExp prec = cite.next();
                            if (addConditionEdge(node, k, prec, action.getAction().getInstantiations(), false, false)) edge_created = true;
                        }
                        cite = action.getAction().getNeg_pred_atend().iterator();
                        while (cite.hasNext()) {
                            IntExp prec = cite.next();
                            if (addConditionEdge(node, k, prec, action.getAction().getInstantiations(), true, false)) edge_created = true;
                        }
                        break;

                    default:

                        break;
                }

                addInterferenceEdges(action, node, k);


                if(!edge_created && node.getType() == 1) {
                    tgraph.addEdge(start, node, new TemporalEdge(minValue, Double.MAX_VALUE, 1) );
                }

            }
        }
    }

    public void addNode (TemporalNode node) {
        tgraph.addVertex(node);
        graphSize++;
    }

    public TemporalNode getNode (int id) {
        Set<TemporalNode> nodes = tgraph.vertexSet();
        Iterator<TemporalNode> itn = nodes.iterator();
        while(itn.hasNext()) {
            TemporalNode node = itn.next();
            if(node.getId() == id)
                return node;
        }
        return null;
    }

    public boolean addConditionEdge (TemporalNode node, Double time, IntExp cond, int [] instantiation, boolean negative, boolean overall) {
        Set<Double> ks = orderedActions.keySet();
        List<Double> keys = new ArrayList<Double>(ks);
        ReverseListIterator itk = new ReverseListIterator(keys);
        while(itk.hasNext()) {
            Double k = (Double) itk.next();
            if (k <= time) {
                Set<ExtIntOp> ita = orderedActions.get(k);
                Iterator<ExtIntOp> acs = ita.iterator();
                while (acs.hasNext()) {
                    ExtIntOp action2 = acs.next();
                    if (action2.getId() != node.getId()) {
                        TemporalNode node2 = getNode(action2.getId());
                        if (satisfyPrecondition(cond, instantiation, action2.getAction(), node2.getType(), negative) && node2.getId() > 0) {
                            tgraph.addEdge(node2, node, new TemporalEdge(minValue, Double.MAX_VALUE, 1));
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public boolean addInterferenceEdges (ExtIntOp action, TemporalNode node, Double time) {
        boolean edge_added = false;
        Set<Double> ks = orderedActions.keySet();
        List<Double> keys = new ArrayList<Double>(ks);
        ReverseListIterator itk = new ReverseListIterator(keys);
        while (itk.hasNext()) {
            Double k = (Double) itk.next();
            if ( k <= time) {
                Set<ExtIntOp> ita = orderedActions.get(k);
                Iterator<ExtIntOp> acs = ita.iterator();
                while (acs.hasNext()) {
                    ExtIntOp action2 = acs.next();
                    boolean interferes = false;
                    // for this pair of nodes check if current node interferes with previous node
                    TemporalNode node2 = null;
                    if (action2.getId() != node.getId()) {
                        node2 = getNode(action2.getId());
                        if(node2.getType() == 1) {
                            // determine if current_node negates at start condition of action start node
                            Iterator<IntExp> cit = action2.getAction().getPos_pred_atstart().iterator();
                            while (cit.hasNext()){
                                IntExp cond2 = cit.next();
                                interferes = interferes || satisfyPrecondition(cond2, action2.getAction().getInstantiations(), action.getAction(), action.getType(), true);
                            }
                            cit = action2.getAction().getNeg_pred_atstart().iterator();
                            while (cit.hasNext()){
                                IntExp cond2 = cit.next();
                                interferes = interferes || satisfyPrecondition(cond2, action2.getAction().getInstantiations(), action.getAction(), action.getType(), false);
                            }

                            // determine if previous node start effect interferes with current node effects
                            cit = action2.getAction().getPos_eff_atstart().iterator();
                            while (cit.hasNext()){
                                IntExp cond2 = cit.next();
                                interferes = interferes || satisfyPrecondition(cond2, action2.getAction().getInstantiations(), action.getAction(), action.getType(), true);
                            }
                            cit = action2.getAction().getNeg_eff_atstart().iterator();
                            while (cit.hasNext()){
                                IntExp cond2 = cit.next();
                                interferes = interferes || satisfyPrecondition(cond2, action2.getAction().getInstantiations(), action.getAction(), action.getType(), false);
                            }

                        } else if (node2.getType() == 2) {

                            // determine if current_node negates at end condition of action end node
                            Iterator<IntExp> cit = action2.getAction().getPos_pred_atend().iterator();
                            while (cit.hasNext()){
                                IntExp cond2 = cit.next();
                                interferes = interferes || satisfyPrecondition(cond2, action2.getAction().getInstantiations(), action.getAction(), action.getType(), true);
                            }
                            cit = action2.getAction().getNeg_pred_atend().iterator();
                            while (cit.hasNext()){
                                IntExp cond2 = cit.next();
                                interferes = interferes || satisfyPrecondition(cond2, action2.getAction().getInstantiations(), action.getAction(), action.getType(), false);
                            }

                            // determine if current_node negates over all condition of action end node
                            cit = action2.getAction().getPos_pred_overall().iterator();
                            while (cit.hasNext()){
                                IntExp cond2 = cit.next();
                                interferes = interferes || satisfyPrecondition(cond2, action2.getAction().getInstantiations(), action.getAction(), action.getType(), true);
                            }
                            cit = action2.getAction().getNeg_pred_overall().iterator();
                            while (cit.hasNext()){
                                IntExp cond2 = cit.next();
                                interferes = interferes || satisfyPrecondition(cond2, action2.getAction().getInstantiations(), action.getAction(), action.getType(), false);
                            }

                            // determine if previous node end effect interferes with current node effects
                            cit = action2.getAction().getPos_eff_atend().iterator();
                            while (cit.hasNext()){
                                IntExp cond2 = cit.next();
                                interferes = interferes || satisfyPrecondition(cond2, action2.getAction().getInstantiations(), action.getAction(), action.getType(), true);
                            }
                            cit = action2.getAction().getNeg_eff_atend().iterator();
                            while (cit.hasNext()){
                                IntExp cond2 = cit.next();
                                interferes = interferes || satisfyPrecondition(cond2, action2.getAction().getInstantiations(), action.getAction(), action.getType(), false);
                            }
                        }
                    }

                    if (interferes && node2.getId() > 0) {
                        tgraph.addEdge(node2, node, new TemporalEdge(minValue, Double.MAX_VALUE, 3));
                        edge_added = true;
                    }

                }
            }
        }
        return edge_added;
    }


    public boolean satisfyPrecondition(IntExp cond, int [] instantiation, IntOp action, int type, boolean negative) {
        if (!negative) {
            if (type == 1) {                        //action start
                Iterator<IntExp> cit = action.getPos_eff_atstart().iterator();
                while(cit.hasNext()){
                    if (domainFormulaMatches(cond, cit.next(), instantiation, action.getInstantiations())) return true;
                }
            } else if (type == 2) {                 //action end
                Iterator<IntExp> cit = action.getPos_eff_atend().iterator();
                while(cit.hasNext()){
                    if (domainFormulaMatches(cond, cit.next(), instantiation, action.getInstantiations())) return true;
                }
            }

        } else {
            if (type == 1) {                        //action start
                Iterator<IntExp> cit = action.getNeg_eff_atstart().iterator();
                while(cit.hasNext()){
                    if (domainFormulaMatches(cond, cit.next(), instantiation, action.getInstantiations())) return true;
                }
            } else if (type == 2) {                 //action end
                Iterator<IntExp> cit = action.getNeg_eff_atend().iterator();
                while(cit.hasNext()){
                    if (domainFormulaMatches(cond, cit.next(), instantiation, action.getInstantiations())) return true;
                }
            }
        }


        return false;
    }



    public boolean domainFormulaMatches (IntExp cond1, IntExp cond2, int [] inst1, int [] inst2) {
        if (cond1.getPredicate() == cond2.getPredicate()) {  //same predicate
            int number_params = cond1.getArguments().length;
            int counter = 0;
            while (counter < number_params) {
                if (inst1[(Math.abs(cond1.getArguments()[counter]) - 1)] != inst2[(Math.abs(cond2.getArguments()[counter]) - 1)]  )
                    return false;                       //some of the grounded parameters are different
                counter++;
            }
            return true;
        }
        return false;
    }




    public void printOrderedActions () {
        Set<Double> keys = orderedActions.keySet();
        Iterator<Double> itk = keys.iterator();
        while(itk.hasNext()) {
            Double k = itk.next();
            Set<ExtIntOp> ita = orderedActions.get(k);
            Iterator<ExtIntOp> acs = ita.iterator();
            while(acs.hasNext()){
                ExtIntOp buff = acs.next();
                System.out.println("Time: " + k + " action: " + buff.getAction().getName() + "inst: " + buff.getAction().printInst() +  " type: " + buff.getType() + " id: " + buff.getId());
            }
        }
    }

    public Set<TemporalEdge> getInputEdges (TemporalNode node) {
        return tgraph.incomingEdgesOf(node);
    }

    public Set<TemporalEdge> getOutputEdges (TemporalNode node) {
        return tgraph.outgoingEdgesOf(node);
    }

    public void normalizeActions () {
        Set<Double> keys = orderedActions.keySet();
        Iterator<Double> itk = keys.iterator();
        while(itk.hasNext()) {
            Double k = itk.next();
            Set<ExtIntOp> ita = orderedActions.get(k);
            Iterator<ExtIntOp> acs = ita.iterator();
            while(acs.hasNext()){
                ExtIntOp buff = acs.next();
                normalizeAction(buff.getAction(), buff.getType());
            }
        }
    }

    public boolean containsNode (TemporalNode node) {
        return tgraph.containsVertex(node);
    }

    public void removeNode (TemporalNode node) {
        tgraph.removeVertex(node);
    }

    public void normalizeAction (IntOp action, int type) {

        if (type==1) {
            List<IntExp> pos_start = action.getPos_pred_atstart();
            Iterator<IntExp> pre_iterator = pos_start.iterator();
            while (pre_iterator.hasNext()) {
                IntExp e = pre_iterator.next();
                int[] arg = new int[e.getArguments().length];
                int x;
                for (int i = 0; i < arg.length; i++) {
                    arg[i] = action.getInstantiations()[Math.abs(e.getArguments()[i]) - 1];
                }
                e.setArguments(arg);
            }

            List<IntExp> neg_start = action.getNeg_pred_atstart();
            pre_iterator = neg_start.iterator();
            while (pre_iterator.hasNext()) {
                IntExp e = pre_iterator.next();
                int[] arg = new int[e.getArguments().length];
                int x;
                for (int i = 0; i < arg.length; i++) {
                    arg[i] = action.getInstantiations()[Math.abs(e.getArguments()[i]) - 1];
                }
                e.setArguments(arg);
            }

            List<IntExp> pos_start_eff = action.getPos_eff_atstart();
            pre_iterator = pos_start_eff.iterator();
            while (pre_iterator.hasNext()) {
                IntExp e = pre_iterator.next();
                int[] arg = new int[e.getArguments().length];
                int x;
                for (int i = 0; i < arg.length; i++) {
                    arg[i] = action.getInstantiations()[Math.abs(e.getArguments()[i]) - 1];
                }
                e.setArguments(arg);
            }

            List<IntExp> neg_start_eff = action.getNeg_eff_atstart();
            pre_iterator = neg_start_eff.iterator();
            while (pre_iterator.hasNext()) {
                IntExp e = pre_iterator.next();
                int[] arg = new int[e.getArguments().length];
                int x;
                for (int i = 0; i < arg.length; i++) {
                    arg[i] = action.getInstantiations()[Math.abs(e.getArguments()[i]) - 1];
                }
                e.setArguments(arg);
            }

            List<IntExp> pos_overall = action.getPos_pred_overall();
            pre_iterator = pos_overall.iterator();
            while (pre_iterator.hasNext()) {
                IntExp e = pre_iterator.next();
                int[] arg = new int[e.getArguments().length];
                int x;
                for (int i = 0; i < arg.length; i++) {
                    arg[i] = action.getInstantiations()[Math.abs(e.getArguments()[i]) - 1];
                }
                e.setArguments(arg);
            }



            List<IntExp> neg_overall = action.getNeg_pred_overall();
            pre_iterator = neg_overall.iterator();
            while (pre_iterator.hasNext()) {
                IntExp e = pre_iterator.next();
                int[] arg = new int[e.getArguments().length];
                int x;
                for (int i = 0; i < arg.length; i++) {
                    arg[i] = action.getInstantiations()[Math.abs(e.getArguments()[i]) - 1];
                }
                e.setArguments(arg);
            }




        } else if (type == 2) {

            List<IntExp> pos_end = action.getPos_pred_atend();
            Iterator<IntExp> pre_iterator = pos_end.iterator();
            while (pre_iterator.hasNext()) {
                IntExp e = pre_iterator.next();
                int[] arg = new int[e.getArguments().length];
                int x;
                for (int i = 0; i < arg.length; i++) {
                    arg[i] = action.getInstantiations()[Math.abs(e.getArguments()[i]) - 1];
                }
                e.setArguments(arg);
            }

            List<IntExp> neg_end = action.getNeg_pred_atend();
            pre_iterator = neg_end.iterator();
            while (pre_iterator.hasNext()) {
                IntExp e = pre_iterator.next();
                int[] arg = new int[e.getArguments().length];
                int x;
                for (int i = 0; i < arg.length; i++) {
                    arg[i] = action.getInstantiations()[Math.abs(e.getArguments()[i]) - 1];
                }
                e.setArguments(arg);
            }

            List<IntExp> pos_end_eff = action.getPos_eff_atend();
            pre_iterator = pos_end_eff.iterator();
            while (pre_iterator.hasNext()) {
                IntExp e = pre_iterator.next();
                int[] arg = new int[e.getArguments().length];
                int x;
                for (int i = 0; i < arg.length; i++) {
                    arg[i] = action.getInstantiations()[Math.abs(e.getArguments()[i]) - 1];
                }
                e.setArguments(arg);
            }

            List<IntExp> neg_end_eff = action.getNeg_eff_atend();
            pre_iterator = neg_end_eff.iterator();
            while (pre_iterator.hasNext()) {
                IntExp e = pre_iterator.next();
                int[] arg = new int[e.getArguments().length];
                int x;
                for (int i = 0; i < arg.length; i++) {
                    arg[i] = action.getInstantiations()[Math.abs(e.getArguments()[i]) - 1];
                }
                e.setArguments(arg);
            }
        }
        //return action;
    }



    public IntOp nodeToAction (double time,int id) {
        Set<ExtIntOp> actions = orderedActions.get(time);
        Iterator<ExtIntOp> it = actions.iterator();
        while(it.hasNext()){
            ExtIntOp eop = it.next();
            if(eop.getId() == id)
                return eop.getAction();
        }
        System.out.println("ACTION NOT FOUND");
        return null;
    }

    public void relaxPlanStartingTime () {
        TemporalNode planStart = getNode(0);

        planStart.setRelaxedTime(0);


        //relaxNodeStartingTime(planStart,0);

        planStart.setRelaxedTime(0);
        Set<TemporalEdge> outEdges = getOutputEdges(planStart);
        Iterator<TemporalEdge> it = outEdges.iterator();
        //System.out.println("node " + node.print() + " has " + outEdges.toString());
        while (it.hasNext()) {
            TemporalEdge e = it.next();
            relaxNodeStartingTime(e, getTarget(e), e.getLb() + planStart.getRelaxedTime());
        }


    }

    public void relaxNodeStartingTime (TemporalEdge e, TemporalNode node, double time) {
        boolean alreadyEnabled = e.getEnabled();
        e.enable();
        updateEdge(getSource(e), getTarget(e), e);

        boolean newRelaxedTime = false;
        boolean ready = true;
        //System.out.println("relatexedTime: " + node.getRelaxedTime() + " and time " + time );

        if (node.getRelaxedTime() < time) {
            node.setRelaxedTime(time);
            newRelaxedTime = true;
        }
        Set<TemporalEdge> inEdges = getInputEdges(node);
        Iterator<TemporalEdge> it = inEdges.iterator();
        while(it.hasNext() && ready){
            TemporalEdge edge = it.next();
            if(!edge.getEnabled()) {
                ready = false;
                //System.out.println("edge " + e.toString() + " is not ready");
            }
        }

        boolean firstTimeReady = ready && !alreadyEnabled;

        if ( firstTimeReady || (!firstTimeReady && newRelaxedTime)  ) {  //either all the input edges just enabled (like in the old version), so I can start traverse the output edges, or they were already enabled and I came back here due to back-forward propagation: in such case I keep propagating forward only if the relatexTime changed

            //dispatching the node

            //if it is an output edge, I need to backpropagate its duration to set a possible new min_relaxed_time for its starting node (may be computationally heavy)
            //todo: investigate if there ara better algorithm to do that (maybe propagate a edges disabling without analyzing the nodes)
            if (node.getType() == 2) {
                Set<TemporalEdge> inDurationEdges = getInputEdges(node);
                Iterator<TemporalEdge> itd = inDurationEdges.iterator();
                while(itd.hasNext()){
                    TemporalEdge ed = itd.next();
                    if(ed.getType() == 2) { //duration edge
                        relaxNodeStartingTime(ed, getSource(ed), node.getRelaxedTime() - ed.getLb());     //backpropagate action duration t, because starting node can not start at time relaxtedTime(endNode) - t)
                        break; //we know there is only 1 incoming duration edge
                    }
                }
            }


            //enabling outpud edges
            Set<TemporalEdge> outEdges = getOutputEdges(node);
            it = outEdges.iterator();
            //System.out.println("node " + node.print() + " has " + outEdges.toString());
            while (it.hasNext()) {
                TemporalEdge edge = it.next();
                //e.enable();
                //updateEdge(getSource(e), getTarget(e), e);
                //relaxNodeStartingTime(getTarget(e),e.getLb() + node.getRelaxedTime());
                relaxNodeStartingTime(edge, getTarget(edge), edge.getLb() + node.getRelaxedTime());
            }

        }
    }



    public int getOrderIdFromNode(TemporalNode failedNode) {
        IntOp action = nodeToAction(failedNode.getTime(),failedNode.getId());
        String name = action.getName();
        int orderIdIndex;
        int orderId;
        switch (name) {
            case "getbasefrombscriticaltask":
                orderIdIndex =  action.getInstantiations()[2];
                orderId = Integer.parseInt(cp.getConstants().get(orderIdIndex).substring(1));
            break;

            case "deliverproductc0tocscriticaltask":

            case "deliverproductc1tocscriticaltask":

            case "deliverproductc2tocscriticaltask":

            case "deliverproductc3tocscriticaltask":
                orderIdIndex =  action.getInstantiations()[4];
                orderId = Integer.parseInt(cp.getConstants().get(orderIdIndex).substring(1));
            break;

            case "getproductfromcscriticaltask":
                orderIdIndex =  action.getInstantiations()[3];
                orderId = Integer.parseInt(cp.getConstants().get(orderIdIndex).substring(1));
            break;

            case "deliverproducttodscriticaltask":
                orderIdIndex =  action.getInstantiations()[2];
                orderId = Integer.parseInt(cp.getConstants().get(orderIdIndex).substring(1));
            break;

            case "deliverproducttors1criticaltask":
                orderIdIndex =  action.getInstantiations()[3];
                orderId = Integer.parseInt(cp.getConstants().get(orderIdIndex).substring(1));
            break;

            case "deliverproducttors2criticaltask":

            case "deliverproducttors3criticaltask":
                orderIdIndex =  action.getInstantiations()[3];
                orderId = Integer.parseInt(cp.getConstants().get(orderIdIndex).substring(1));
            break;

            case "getproductfromrscriticaltask":
                orderIdIndex =  action.getInstantiations()[4];
                orderId = Integer.parseInt(cp.getConstants().get(orderIdIndex).substring(1));
            break;

            default:
                orderId = 0;
            break;

        }
        return orderId;
    }

    public void printGraph () {
        System.out.println(tgraph.toString());
    }

    public void updateEdge (TemporalNode source, TemporalNode target, TemporalEdge e) {
        tgraph.addEdge(source,target,e);
    }

    public TemporalNode getSource (TemporalEdge e){
        return tgraph.getEdgeSource(e);
    }

    public TemporalNode getTarget (TemporalEdge e) {
        return tgraph.getEdgeTarget(e);
    }

    public double calculateRelaxedMakespan () {
        double makespan = -1;
        Set<TemporalNode> allNodes = tgraph.vertexSet();

        for (TemporalNode node : allNodes) {
            if (node.getType() == 1) {
                double time = node.getRelaxedTime();
                if (time > makespan)
                    makespan = time;
            }
        }
        return makespan;
    }

    public void createRelaxedPlanFile(String rawRefinedDomainFile) {
        File output = new File( rawRefinedDomainFile.replace("rawRefined","relaxed"));
        try {
            FileWriter fr = new FileWriter(output, false);

            Set<TemporalNode> allNodes = tgraph.vertexSet();
            Set<TemporalNode> startingNodes = new HashSet<>();

            //keep only starting node
            for (TemporalNode node : allNodes) {
                if (node.getType() == 1)
                    startingNodes.add(node);
            }

            Set<TemporalNode> alreadyPrinted = new HashSet<>();
            boolean finish = false;
            TemporalNode minNode = null;
            while (!finish) {
                double minTime = 1000000;
                finish = true;
                for (TemporalNode node : startingNodes) {
                    if (!alreadyPrinted.contains(node)) {
                        //System.out.println(node + " is not inside alreadyprinted " + alreadyPrinted);
                        finish = false;
                        double time = node.getRelaxedTime();
                        if (time <= minTime) {
                            minTime = time;
                            minNode = node;
                        }
                    }
                }
                if (!finish) {
                    fr.write(minNode.prettyPrint() + "\n");
                    //fr.write(minNode.print() + "\n");
                    alreadyPrinted.add(minNode);
                    //System.out.println("already printed is " + alreadyPrinted);
                }
            }



            fr.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }




    }

    public int calculateDeadlinePenalty(int currentTime) {
        int penalty = 0;
        TemporalNode planStart = getNode(0);
        HashMap<Integer, Double> nodeDl = planStart.getSoftDeadline();
        for (int order: nodeDl.keySet()) {

            if (currentTime > nodeDl.get(order)) {
                Order o = refboxClient.getOrderById(order);
                log.warn("Soft deadline " + nodeDl.get(order) + " for order " + order + " will be violated (current time: " + currentTime + " ), assigning penalty");
                double penaltyPercentage = calculateDeadlinePenaltyPercentage(o.getDeliveryPeriodBegin(), o.getDeliveryPeriodEnd() , currentTime - nodeDl.get(order) );
                ProductComplexity complexity = o.getComplexity();
                switch (complexity) {

                    case C0:
                        penalty += penaltyPercentage * 32;
                    break;

                    case C1:
                        penalty += penaltyPercentage * 54;
                        break;

                    case C2:
                        penalty += penaltyPercentage * 87;
                        break;

                    case C3:
                        penalty += penaltyPercentage * 142;
                        break;

                }
            }
        }


        return penalty;
    }

    private double calculateDeadlinePenaltyPercentage(int lDeadline, int uDeadline, Double late) {
        double penaltyPercentage = 15; //basic penalty
        int block = (uDeadline - lDeadline) / 5;
        penaltyPercentage += 5 * (late / ( (uDeadline - lDeadline) / 5));
        if (penaltyPercentage > 75)
            penaltyPercentage = 75;
        //log.info("penalty percentage is " + penaltyPercentage);
        return penaltyPercentage * 0.01;
    }
}
