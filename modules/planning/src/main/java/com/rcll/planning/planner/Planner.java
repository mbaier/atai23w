package com.rcll.planning.planner;

import com.rcll.planning.encoding.RcllCodedProblem;
import com.rcll.planning.encoding.IntOp;
import com.rcll.planning.util.RcllTemporalPlan;
import lombok.extern.apachecommons.CommonsLog;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.*;

@CommonsLog
public class Planner {
    private boolean firstPlanFound;
    private String domain;

    private String macroDomain;
    private String problem;
    private boolean finish;
    private boolean actuallyFinish;
    private final RcllTemporalPlan plan;
    private final RcllCodedProblem cp;
    private final String plannerBinary;



    public Planner(String domain, String problem, RcllCodedProblem cp, String plannerBinary){
        this.domain = domain;
        this.problem = problem;
        this.cp = cp;
        this.plannerBinary = plannerBinary;
        plan = new RcllTemporalPlan();
        firstPlanFound = false;
    }

    public Planner(String domain, String macroDomain, String problem, RcllCodedProblem cp, String plannerBinary){
        this.domain = domain;
        this.macroDomain = macroDomain;
        this.problem = problem;
        this.cp = cp;
        this.plannerBinary = plannerBinary;
        plan = new RcllTemporalPlan();
        firstPlanFound = false;
    }

    public ArrayList<Integer> TSPplan () {
        String s;
        Process p = null;
        boolean parseAction = false;
        boolean parseTime = false;
        boolean finish = false;
        ArrayList<Integer> orderToVisit = new ArrayList<Integer>();

        try {
            p = Runtime.getRuntime().exec(plannerBinary + " " + domain + " " + problem);
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(p.getInputStream()));

            List<IntOp> op = cp.getOperators();
            List<String> constants = cp.getConstants();
            boolean skipline = false;
            while ((s = br.readLine()) != null) {
                String[] tokens = s.split(" ");
                if (tokens.length == 3)
                    if (tokens[1].equals("Solution") && tokens[2].equals("Found") ) {
                        System.out.println("solution found");
                        break;
                    }

            }

            while ((s = br.readLine()) != null && !finish ) {
                String[] tokens = s.split(" ");
                if (tokens.length > 2 && parseTime)
                    if (!tokens[1].equals("Dummy"))
                        parseAction = true;
                if (parseAction) {
                    if (s.equals("")) {
                        parseAction = false;
                        finish = true;
                    }
                    else {
                        String [] parsed = s.split(":");
                        int zoneIndex = parsed[1].indexOf(")") - 1;
                        char zoneName = parsed[1].charAt(zoneIndex);
                        orderToVisit.add(Character.getNumericValue(zoneName));
                        skipline = true;
                    }
                }
                if (tokens.length > 2 && !skipline) {
                    if (tokens[1].equals("Dummy") && tokens[2].equals("steps:"))
                        parseAction = true;
                    else if (tokens[1].equals("Time"))
                        parseTime = true;
                }
                System.out.println("line: " + s);
            }
            //p.waitFor();
            log.info("exit: " + p.exitValue());
            p.destroy();
            return orderToVisit;
        } catch (Exception e) { log.error("Exception during planning call: " + e);
            p.destroy();
            return orderToVisit;
        }
    }

    public void plan ( String threadNumber){
        //cleanOldSolutionFiles();


        this.problem =  "problem" + threadNumber +  ".pddl";
        //this.problem = "/tempo-sat-temporal-fast-downward/p05";

        String s;
        Process p = null;
        finish = false;
        actuallyFinish = false;

        //swapping robotIds of the plan such that first action is executed by R1, second action by R2, ecc.."
        int checkedRobots = 0;
        int [] robotMap = new int[3];
        robotMap[0] = -1;
        robotMap[1] = -1;
        robotMap[2] = -1;




        try {
            firstPlanFound = false;
            int lastPlanningTime = findLastPlanningTimeFolder(threadNumber);
            if (lastPlanningTime == -1) {
                log.error("Last planning folder not found");
                throw new Exception("Last planning folder not found");
            }
            File workingDir = new File("problem" + threadNumber);           //remove this line with care, multiple threads working on the same working dir may cause planning errors
            File newPlanningFolder = new File("problem" + threadNumber + "/planning" + (lastPlanningTime+1));
            newPlanningFolder.mkdir();

            String command = "";
            command =  plannerBinary + " domain.pddl " + problem + " planning" + (lastPlanningTime+1) + "/sol" + threadNumber;


            log.info("Running command in [" + workingDir + "]: " + command);
            p = Runtime.getRuntime().exec(  command, null, workingDir);
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(p.getInputStream()));

            //List<IntOp> op = cp.getOperators();
            //List<String> constants = cp.getConstants();

            Timer timer;
            timer = new Timer();
            timer.schedule(new PlanningTimeout(p), 60000);
            //timer.schedule(new PlanningTimeout(p), 3);

            firstPlanFound = false;
            while ((s = br.readLine()) != null && !finish ) {
                if (s.contains("Found new plan:")) {
                    firstPlanFound = true;
                    log.info("++++++++ PLAN FOUND IN THREAD " + threadNumber);
                }
            }
            log.info("Out of planning loop");

            p.destroy();
            p.waitFor();

            actuallyFinish = true;

        } catch (Exception e) { log.error("Exception during planning call: " + e); e.printStackTrace();
            p.destroy();
            try {
                p.waitFor();
            } catch (InterruptedException ex) {
                throw new RuntimeException(ex);
            }
            actuallyFinish = true;
        }
    }

    private int findLastPlanningTimeFolder(String threadNumber) {
        String[] pathnames;

        File f = new File("problem" + threadNumber);
        pathnames = f.list();

        int lastPlanningTime = -1;

        for (String pathname : pathnames) {
            //log.warn("pathname " + pathname);
            // Print the names of files and directories
            if (pathname.contains("planning")) {
                int candidate = Integer.parseInt(pathname.substring(8));
                if (candidate > lastPlanningTime)
                    lastPlanningTime = candidate;
            }
        }
        return lastPlanningTime;
    }


    public void printPlan (RcllCodedProblem rcp) {
        System.out.println(this.planToString(rcp));
    }

    public String planToString(RcllCodedProblem rcp) {
        StringWriter writer = new StringWriter();
        TreeMap<Double, Set<IntOp>> actions = plan.actions();
        List<String> constants = rcp.getConstants();
        for (Map.Entry<Double, Set<IntOp>> entry : actions.entrySet()) {
            writer.append(String.valueOf(entry.getKey())).append(" ");
            Set<IntOp> seti = entry.getValue();
            Iterator<IntOp> setIt = seti.iterator();
            while(setIt.hasNext()){
                IntOp i = setIt.next();
                writer.append(i.getName()).append("(");
                for(int j=0; j < i.getInstantiations().length; j++)
                    if (j==i.getInstantiations().length-1)
                        writer.append(constants.get(i.getInstantiations()[j]));
                    else
                        writer.append(constants.get(i.getInstantiations()[j])).append(", ");
                writer.append(") [").append(String.valueOf(i.getDuration())).append("]\n");
            }
        }
        if (actions.size() == 0)
            writer.append("++++++++++++++++EMPTY PLAN++++++++++++++++++");
        return writer.toString();
    }

    public void addActionToPlan (Double time, IntOp grAction) {
        plan.add(time, grAction);
    }

    public RcllTemporalPlan getPlan () {
        return this.plan;
    }

    public class PlanningTimeout extends TimerTask {

        Process p;

        public PlanningTimeout(Process p) {
            this.p = p;
        }

        @Override
        public void run() {
            if (p.isAlive()) {
                firstPlanFound = true;  //not a typo, needed for therad sync in caso a solution is not found by any thread
                log.warn("Planning out of time, interrupting");
                p.destroy();
                try {
                    p.waitFor();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                actuallyFinish = true;
            }
        }
    }


}
