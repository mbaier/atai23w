#!/bin/bash
echo "Removing old logs folder"
rm -rf logs
echo "Creating new Logs folder"
mkdir logs
echo "Copying the Simulator logs!"
docker cp simulation:simulator/logs ./logs/Simulator
echo "Copying the Simulator logs!"
docker cp refbox:/mps ./logs/Refbox
docker cp -L refbox:refbox-debug_latest.log ./logs/Refbox/
docker cp -L refbox:refbox_latest.log ./logs/Refbox/
echo "Copying the Team Server log!"
mkdir logs/Team_server
docker logs grips_team_server >& logs/Team_server/team_server.log
