package com.grips;

import com.grips.persistence.misc.GripsDataService;
import com.sun.xml.bind.v2.model.core.ID;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class FakeGripsDataService<T, ID> implements GripsDataService<T, ID> {
    private final Map<ID, T> data;

    public FakeGripsDataService() {
        this.data = new HashMap();
    }

    @Override
    public boolean existsById(ID id) {
        return this.data.containsKey(id);
    }

    @Override
    public Optional<T> findById(ID id) {
        return Optional.ofNullable(this.data.get(id));
    }

    @Override
    public <S extends T> S save(S entity) {
        try {
            Method method = entity.getClass().getMethod("getId");
            ID id = (ID) method.invoke(entity);
            if (id == null) {
                throw new IllegalArgumentException("Id not set, not yet supported in FakeGripsDataServie!");
            }
            this.data.put(id, entity);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
        return entity;
    }
}
