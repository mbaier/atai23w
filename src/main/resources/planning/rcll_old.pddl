(define (domain rcll)
 (:requirements :strips :typing :durative-actions :fluents)
 (:types base_station  - location
   location  - station
   robot product )

  (:predicates
     (at ?r - robot ?l - location)
     (free ?r - robot)
     (empty ?l - location)
	 (delivered ?p - product)
 )

 
 

(:durative-action toyActionDeliveringFromBs
	:parameters (?r - robot  ?p - product ?bs - base_station)
	:duration (= ?duration  10)
	:condition (and
		(at start (free ?r))
		(at start (at ?r ?bs))
	)
	:effect (and
		(at start (not (free ?r)))
		(at end (free ?r))
		(at end (delivered ?p ))
 	)
)

(:durative-action move
	:parameters (?r - robot ?from - location  ?to - location )
	:duration (= ?duration 10)
	:condition (and (at start (at ?r ?from)) (at start (free ?r)) (at start (empty ?to) )  )
	:effect (and
             (at start (not (at ?r ?from)))
             (at end (at ?r ?to))
	     (at start (not (free ?r)))
	     (at end (free ?r))
	     (at start (not (empty ?to)))
	     (at start (empty ?from))
 	)
)








)
