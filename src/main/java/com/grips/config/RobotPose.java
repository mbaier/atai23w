package com.grips.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "robotpose")
@Getter
@Setter

public class RobotPose {
    private float x;
    private float y;
    private float yaw;
}
