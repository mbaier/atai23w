/*
 *
 * Copyright (c) 2017, Graz Robust and Intelligent Production System (grips)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.grips.config;

import com.grips.persistence.dao.BeaconSignalFromRobotDao;
import com.grips.persistence.dao.MachineInfoRefBoxDao;
import com.grips.persistence.dao.SubProductionTaskDao;
import com.grips.refbox.PrivateRefBoxHandler;
import com.grips.refbox.PublicRefBoxHandler;
import com.grips.robot.HandleRobotMessageThreadFactory;
import com.grips.robot.RobotClientWrapper;
import com.grips.scheduler.EmptyScheduler;
import com.grips.scheduler.GameField;
import com.grips.scheduler.api.IScheduler;
import com.grips.scheduler.asp.*;
import com.grips.scheduler.asp.dispatcher.ActionMapping;
import com.grips.scheduler.challanges.GraspingScheduler;
import com.grips.scheduler.challanges.NavigationData;
import com.grips.scheduler.challanges.NavigationScheduler;
import com.grips.scheduler.exploration.PreProductionService;
import com.rcll.protobuf_lib.RobotConnections;
import com.rcll.refbox.PeerConfig;
import com.rcll.refbox.RefboxClient;
import com.rcll.refbox.RefboxConnectionConfig;
import com.rcll.refbox.TeamConfig;
import com.rcll.robot.RobotHandler;
import com.robot_communication.services.GripsRobotClient;
import com.robot_communication.services.GripsRobotTaskCreator;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.util.Optional;

@Configuration
@CommonsLog
public class Config {
    public static final long NUM_MILIS_ROBOT_LOST = 5000;

    public static final int NUM_RING_COLORS = 4;

    public static int ROBOT_LISTEN_PORT = 2016;
    public static String TEAM_NAME = "GRIPS";
    public static String TEAM_CYAN_COLOR = "CYAN";

    @Value("${gameconfig.robot.recvport}")
    public void setRobotListenPort(int port) {
        ROBOT_LISTEN_PORT = port;
    }

    @Bean()
    public RobotConnections create_RobotConnections() {
        return new RobotConnections((int) NUM_MILIS_ROBOT_LOST);
    }

    @Bean()
    public RefboxClient create_RefboxClient(RefboxConfig config,
                                            PrivateRefBoxHandler teamHandler,
                                            PublicRefBoxHandler publicHandler) {
        RefboxConnectionConfig connectionConfig = new RefboxConnectionConfig(
                config.getIp(),
                new PeerConfig(config.getPublic_port_receive(), config.getPublic_port_send()),
                new PeerConfig(config.getPrivate_port_receive_cyan(), config.getPrivate_port_send_cyan()),
                new PeerConfig(config.getPrivate_port_receive_magenta(), config.getPrivate_port_send_magenta()));
        TeamConfig teamConfig = new TeamConfig(config.getCrypto_key(), "GRIPS");
        RefboxClient refboxClient = new RefboxClient(connectionConfig, teamConfig, teamHandler, publicHandler, 2000);
        try {
            refboxClient.startServer();
            log.info("Started RefboxClient Server!");
        } catch (Exception e) {
            log.error("Error starting RefboxConnectionManager - is the refbox running?", e);
        }
        return refboxClient;
    }

    @Bean
    public GripsRobotClient robotClient(GripsRobotTaskCreator robotTaskCreator, RobotConnections robotConnections) {
        return new GripsRobotClient(robotTaskCreator, robotConnections);
    }

    @Bean()
    public GripsRobotTaskCreator prsTaskCreator() {
        return new GripsRobotTaskCreator();
    }

    @Bean
    @Scope("prototype")
    public RobotHandler robotHandler(RobotConnections robotConnections,
                                     HandleRobotMessageThreadFactory robotMessageThreadFactory) {
        return new RobotHandler(robotConnections, robotMessageThreadFactory, (robot) -> log.info("HERE1 adding robot: " + robot));
    }

    @Bean
    @Qualifier("production-scheduler")
    public IScheduler createProductionScheduler(ClassesConfig classesConfig,
                                                GripsRobotClient robotClient,
                                                SubProductionTaskDao subProductionTaskDao,
                                                GripsRobotTaskCreator robotTaskCreator,
                                                RobotClientWrapper robotClientWrapper,
                                                BeaconSignalFromRobotDao beaconSignalFromRobotDao,
                                                RefboxClient refboxClient,
                                                PreProductionService preProductionService,
                                                DistanzeMatrix distanzeMatrix,
                                                KnowledgeBase kb,
                                                RobotStartChecker robotStartChecker,
                                                ActionMapping actionMapping,
                                                NavigationData navigationData,
                                                GameField gameField,
                                                ProductionTimesConfig productionTimesConfig,
                                                MachineInfoRefBoxDao machineInfoRefBoxDao) {
        if (AspScheduler.class.getSimpleName().equalsIgnoreCase(classesConfig.getProductionScheduler())) {
            return new AspScheduler(robotClient, refboxClient,
                    subProductionTaskDao, beaconSignalFromRobotDao, robotClientWrapper, preProductionService,
                     distanzeMatrix, kb, robotStartChecker, actionMapping, gameField, productionTimesConfig,  machineInfoRefBoxDao);
        } else if (GraspingScheduler.class.getSimpleName().equalsIgnoreCase(classesConfig.getProductionScheduler())) {
            return new GraspingScheduler(robotTaskCreator, robotClient);
        } else if (NavigationScheduler.class.getSimpleName().equalsIgnoreCase(classesConfig.getProductionScheduler())) {
            return new NavigationScheduler(robotClient, navigationData);
        } else if (EmptyScheduler.class.getSimpleName().equalsIgnoreCase(classesConfig.getProductionScheduler())) {
            return new EmptyScheduler();
        } else {
            log.error("Invalid Scheduler class: " + Optional.ofNullable(classesConfig.getProductionScheduler()).orElse("NOT SET"));
            System.exit(1);
        }
        return null;
    }

    @Bean
    public ActionMapping createActionMapping(RefboxClient refboxClient, RefboxConfig refboxConfig) {
        return new ActionMapping(refboxClient, refboxConfig);
    }
}
