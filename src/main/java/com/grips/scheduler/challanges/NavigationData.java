package com.grips.scheduler.challanges;

import lombok.Getter;
import org.robocup_logistics.llsf_msgs.NavigationChallengeProtos;
import org.springframework.stereotype.Service;

@Service
@Getter
public class NavigationData {
    private NavigationChallengeProtos.NavigationRoutes routes;

    public void update(NavigationChallengeProtos.NavigationRoutes routes) {
        this.routes = routes;
    }
}
