package com.grips.scheduler.asp.dispatcher;


import com.grips.config.RefboxConfig;
import com.grips.persistence.domain.ProductOrder;
import com.grips.persistence.domain.SubProductionTask;
import com.grips.persistence.domain.SubProductionTaskBuilder;
import com.rcll.domain.*;
import com.rcll.planning.encoding.IntOp;
import com.rcll.refbox.RefboxClient;
import lombok.extern.apachecommons.CommonsLog;
import org.robocup_logistics.llsf_msgs.MachineDescriptionProtos;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

@CommonsLog
public class ActionMapping {

    private final RefboxClient refboxClient;
    private List<String> cpConstants;

    private String colorprefix;

    public ActionMapping(RefboxClient refboxClient, RefboxConfig refboxConfig) {
        this.refboxClient = refboxClient;
    }

    public void updateCodedProblem(List<String> cpConstants) {
        this.cpConstants = cpConstants;
    }

    public SubProductionTask actionToTask (IntOp action) {
        colorprefix = refboxClient.getTeamColor().map(x -> TeamColor.CYAN.equals(x) ? "C-": "M-").orElseThrow();

        String name = action.getName();
        SubProductionTask task;
        int robotIndex =  action.getInstantiations()[0];
        String robotName = cpConstants.get(robotIndex);
        int robotId = Character.getNumericValue(robotName.charAt(robotName.length()-1));

        int locationIndex = action.getInstantiations()[1];
        String location = cpConstants.get(locationIndex).toUpperCase().substring(2); // .replaceAll("[0-9]", "");

        String color = null;
        if (location.contains("_GREY")){
            location = location.replace("_GREY", "");
            color = Cap.Grey.toString();
        }
        if (location.contains("_BLACK")){
            location = location.replace("_BLACK", "");
            color = Cap.Black.toString();
        }

        int sideIndex = action.getInstantiations()[2];
        String side = cpConstants.get(sideIndex);
        MachineSide machineSide;
        switch (side) {
            case "input":
                machineSide = MachineSide.Input;
                break;
            case "output":
                machineSide = MachineSide.Output;    
                break;
            case "slide":
                machineSide = MachineSide.Slide;      
                break;
            default:
                log.error("Side not managed!");
                return null;
        }

        long order = 0;
        if (name.length() > 6 && name.substring(0, 7).equals("deliver") && !name.equals("deliver_to_ring_station_slide")){
            int orderId =  action.getInstantiations()[3];
            String orderName = cpConstants.get(orderId);
            order = Long.parseLong(orderName.substring(1));
        }

        //ATAI parameters that have to be set for specific kind of tasks:
        //.setOrderInfoId((long) orderId) in all tasks in which you are manipulating a subpiece of the product (buffering cs, retrieving the base from the CS after the buffering, retrieving a base for a RS and delivering a base to the RS slide are excluded)
        //.setIsDemandTask(true) for all the tasks excluded by .setOrderInfoId (buffering cs, retrieving the base from the CS after the buffering, retrieving a base for a RS and delivering a base to the RS slide)
        //.setRequiredColor for retrieving a base, delivering partial product to RS to mount a ring, delivering partial product to CS to mount a cap
        //.setOptCode("RETRIEVE_CAP") for the buffer cap action
        //.setOptCode(MachineDescriptionProtos.CSOp.MOUNT_CAP.toString()) when delivering a partial product to a CS to mount a cap
        //.setOptCode(order.getDeliveryGate()+"") when delivering the final product to the DS (order is a ProductOrder object stored in the ProductOrderDAO)

        switch (name) {
            case "move":
            case "move_to_other_side":
            case "move_to_other_location":
                task = SubProductionTaskBuilder.newBuilder()
                        .setName("Move")
                        .setMachine(colorprefix + location)
                        .setState(SubProductionTask.TaskState.TBD)
                        .setType(SubProductionTask.TaskType.MOVE)
                        .setSide(machineSide)
                        .setOrderInfoId(null)
                        .setIsDemandTask(true)
                        .build();
                break;
            case "buffer":
                task = SubProductionTaskBuilder.newBuilder()
                        .setName("Buffer")
                        .setMachine(colorprefix + location)
                        .setState(SubProductionTask.TaskState.TBD)
                        .setType(SubProductionTask.TaskType.BUFFER_CAP)
                        .setSide(machineSide)
                        .setOptCode(CapStationInstruction.RetrieveCap.toString())
                        .setOrderInfoId(null)
                        .setIsDemandTask(true)
                        .build();
                break;
            case "get":
                if (color == null){
                    int assemblyId =  action.getInstantiations()[3];
                    String assemblyName = cpConstants.get(assemblyId);
                    if (assemblyName.contains("red")){
                        color = Base.Red.toString();
                    } else if (assemblyName.contains("black")){
                        color = Base.Black.toString();
                    }
                }
                
                task = SubProductionTaskBuilder.newBuilder()
                        .setName("Get")
                        .setMachine(colorprefix + location)
                        .setState(SubProductionTask.TaskState.TBD)
                        .setType(SubProductionTask.TaskType.GET)
                        .setSide(machineSide)
                        .setRequiredColor(color)
                        .setOrderInfoId(null)
                        .setIsDemandTask(true)
                        .setOptCode(null)
                        .build();
                break;
            case "deliver_to_cap_station":
                task = SubProductionTaskBuilder.newBuilder()
                        .setName("Deliver To Cap Station")
                        .setMachine(colorprefix + location)
                        .setState(SubProductionTask.TaskState.TBD)
                        .setType(SubProductionTask.TaskType.DELIVER)
                        .setSide(machineSide)
                        .setOrderInfoId(order)
                        .setRequiredColor(color)
                        .setOptCode(CapStationInstruction.MountCap.toString())
                        .setIsDemandTask(false)
                        .build();
                break;
            case "deliver_to_ring_station_slide":
                task = SubProductionTaskBuilder.newBuilder()
                        .setName("Deliver To Slide")
                        .setMachine(colorprefix + location)
                        .setState(SubProductionTask.TaskState.TBD)
                        .setType(SubProductionTask.TaskType.DELIVER)
                        .setSide(machineSide)
                        .setOrderInfoId(null)
                        .setIsDemandTask(false)
                        .build();
                break;
            case "deliver_to_ring_station_first_ring":
            case "deliver_to_ring_station_second_ring":
            case "deliver_to_ring_station_third_ring":
                int ringColorId =  action.getInstantiations()[5];
                String ringColorName = cpConstants.get(ringColorId);
                if (ringColorName.contains("blue")){
                    color = RingColor.Blue.toString();
                } else if (ringColorName.contains("green")){
                    color = RingColor.Green.toString();
                } else if (ringColorName.contains("orange")){
                    color = RingColor.Orange.toString();
                } else if (ringColorName.contains("yellow")){
                    color = RingColor.Yellow.toString();
                }
                
                task = SubProductionTaskBuilder.newBuilder()
                        .setName("Deliver To Ring Station")
                        .setMachine(colorprefix + location)
                        .setState(SubProductionTask.TaskState.TBD)
                        .setType(SubProductionTask.TaskType.DELIVER)
                        .setSide(machineSide)
                        .setOrderInfoId(order)
                        .setRequiredColor(color)
                        .setIsDemandTask(false)
                        .build();
                break;
            case "deliver_c0_to_deliver_station":
            case "deliver_c1_to_deliver_station":
            case "deliver_c2_to_deliver_station":
            case "deliver_c3_to_deliver_station":
                Order productOrder = this.refboxClient.getOrderById((int)order);
                task = SubProductionTaskBuilder.newBuilder()
                        .setName("Deliver To Station")
                        .setMachine(colorprefix + location)
                        .setState(SubProductionTask.TaskState.TBD)
                        .setType(SubProductionTask.TaskType.DELIVER)
                        .setSide(machineSide)
                        .setOrderInfoId(order)
                        .setOptCode(productOrder.getDeliveryGate()+"")
                        .setIsDemandTask(false)
                        .build();
                break;

            default:
                System.out.println("case not managed");
                task = null;
                break;
        }
        task.setRobotId(robotId);
        return task;
    }
}
