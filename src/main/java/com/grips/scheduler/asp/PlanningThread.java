package com.grips.scheduler.asp;

import com.grips.config.ProductionTimesConfig;
import com.rcll.domain.*;
import com.grips.persistence.dao.*;
import com.grips.persistence.domain.Atom;
import com.grips.persistence.domain.MachineInfoRefBox;
import com.grips.persistence.domain.ProductOrder;
import com.rcll.planning.planparser.TemporalGraph;
import com.rcll.planning.encoding.RcllCodedProblem;
import com.rcll.planning.encoding.Encoder;
import com.rcll.planning.encoding.IntOp;
import com.rcll.planning.planner.Planner;
import com.rcll.planning.util.TempAtom;
import com.rcll.refbox.RefboxClient;
import fr.uga.pddl4j.parser.Domain;
import fr.uga.pddl4j.parser.Parser;
import fr.uga.pddl4j.parser.Problem;
import lombok.SneakyThrows;
import lombok.extern.apachecommons.CommonsLog;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;

import java.io.*;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

@CommonsLog
public class PlanningThread {
    private final String domainS = "domain.pddl";
    private final String problemS = "problem";

    private final String plannerBinary = "../../tempo-sat-temporal-fast-downward/plan";
    private List<Order> goals;
    private TemporalGraph tgraph;

    private RcllCodedProblem cp;

    private final String threadNumber;
    private double makespawn;
    private final int remainingTime;
    private final boolean simplestPlan;

    private boolean noSolution;

    private Planner pl;

    private final KnowledgeBase kb;
    private final int numberRobots;

    private final MachineInfoRefBoxDao machineInfoRefBoxDao;

    private final ProductionTimesConfig productionTimes;
    private final TeamColor teamcolor;
    private final Map<String, Double> distanceMatrix;

    private final RefboxClient refboxClient;
    private double timeFactor = 0.25;   //approximated map between plan times and real times (not the simulation real time factor)

    public PlanningThread(String threadNumber, int remainingTime,
                          boolean simplestPlan, KnowledgeBase kb, int numberRobots,
                          ProductionTimesConfig productionTimesConfig, MachineInfoRefBoxDao machineInfoRefBoxDao,
                          TeamColor teamcolor, Map<String, Double> distanceMatrix, RefboxClient refboxClient) {
        this.productionTimes = productionTimesConfig;
        this.threadNumber = threadNumber;
        this.remainingTime = remainingTime;
        this.refboxClient = refboxClient;
        this.simplestPlan = simplestPlan;
        noSolution = false;
        this.kb = kb;
        this.numberRobots = numberRobots;
        this.machineInfoRefBoxDao = machineInfoRefBoxDao;
        this.teamcolor = teamcolor;
        this.distanceMatrix = distanceMatrix;
        createFolder();
    }


    public void plan() {
        deleteOldPlanFile();


        pl = null;


        goals = goalSelector();

        log.info("PLANNING");


        generateProblemFile(goals);
        try {
            cp = callExtPlanner();
        } catch (Exception e) {
            if (e instanceof IOException) {
                log.error("Exception " + e.toString());
                makespawn = 100000;
                noSolution = true;
            }
        }

        collectResult();
        try {
            makespawn = tgraph.returnMakespawn();  //check makespan in case of plan not found
        } catch (NullPointerException e) {
            log.warn("Plan not found, setting makespan to 100000");
            makespawn = 100000;
            noSolution = true;
        }

    }

    @SneakyThrows
    public void createFolder() {
        ResourceHelper.copyResourcesFileTo("planning/rcll.pddl", "problem" + threadNumber + "/domain.pddl");
        new File("problem" + threadNumber + "/planning0").mkdirs();
    }

    public void collectResult() {
        String[] pathnames;

        int lastPlanningTime = findLastPlanningTimeFolder();
        File f = new File("problem" + threadNumber + "/planning" + lastPlanningTime);

        String lastPlan;
        int max_plan_index = 0; //plan not found
        pathnames = f.list();

        for (String pathname : pathnames) {
            // log.warn("pathname " + pathname);
            // Print the names of files and directories
            if (pathname.contains("sol")) {
                int plan_index = Integer.parseInt(pathname.substring(5));
                if (plan_index > max_plan_index)
                    max_plan_index = plan_index;
            }
        }

        if (max_plan_index > 0) {
            lastPlan = "/planning" + lastPlanningTime + "/sol" + threadNumber + "." + max_plan_index;
            reformatPlan(lastPlan, lastPlanningTime);
            readPlan(lastPlanningTime);


            Set<Double> keys = pl.getPlan().actions().keySet();
            Iterator<Double> itk = keys.iterator();
            while (itk.hasNext()) {
                Set<IntOp> ita = pl.getPlan().getActionSet(itk.next());
                Iterator<IntOp> acs = ita.iterator();
                while (acs.hasNext()) {
                    IntOp o = acs.next();
                    o.generateLists();
                }
            }
            log.warn("Generating temporal graph \n");
            //plan is represented as a temporal graph inside tg object
            TemporalGraph tg = new TemporalGraph(cp, pl.getPlan(), refboxClient);
            tg.createTemporalGraph();
            tg.normalizeActions();
            // tg.printOrderedActions();
            // tg.printGraph();
            log.warn("finish");
            this.tgraph = tg;
            deleteOldPlanFile();
        } else {
            log.warn("Plan not found in thread " + threadNumber);
        }

    }

    private int findLastPlanningTimeFolder() {
        String[] pathnames;

        File f = new File("problem" + threadNumber);
        pathnames = f.list();

        int lastPlanningTime = -1;

        for (String pathname : pathnames) {
            //log.warn("pathname " + pathname);
            // Print the names of files and directories
            if (pathname.contains("planning")) {
                int candidate = Integer.parseInt(pathname.substring(8));
                if (candidate > lastPlanningTime)
                    lastPlanningTime = candidate;
            }
        }
        return lastPlanningTime;
    }

    private void deleteOldPlanFile() {
        File f = new File("problem" + threadNumber);
        String[] pathnames = f.list();

        if (pathnames == null) {
            log.warn("Old folder not found, skipping deletion");
            return;
        }
        for (String pathname : pathnames) {
            //log.warn("pathname " + pathname);
            // Print the names of files and directories
            if (pathname.equals("output") || pathname.equals("plan") || pathname.equals("variables.groups") || pathname.equals("all.groups") || pathname.equals("output.sas")) {
                File myObj = new File("problem" + threadNumber + "/" + pathname);
                if (myObj.delete()) {
                    log.info("Deleted the file: " + myObj.getName());
                } else {
                    log.warn("Failed to delete the file.");
                }
            }
        }
    }

    private void readPlan(int lastPlanningTime) {
        File file = new File("problem" + threadNumber + "/planning" + lastPlanningTime + "/plan");
        try {
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);  //creates a buffering character input stream
            String s;

            List<IntOp> op = cp.getOperators();
            List<String> constants = cp.getConstants();


            while ((s = br.readLine()) != null) {
                String[] parsed = s.split(":");
                double time = Double.parseDouble(parsed[0]);
                int actionIndex = parsed[1].indexOf(")") + 1;
                String action = parsed[1].substring(1, actionIndex);
                double duration = Double.parseDouble(parsed[1].substring(actionIndex + 2, parsed[1].length() - 1));
                String actionName = action.split(" ")[0].substring(1);
                //adding grounded actions with time and duration
                if (!actionName.equals("waitfordeliverywindow")) {     //this is a toy action to encode temp constraint, not a real action
                    Iterator<IntOp> it = op.iterator();
                    IntOp grAction = null;    
                    while (it.hasNext()) {    
                        IntOp i = it.next();
                        if (i.getName().equals(actionName)) {
                            grAction = new IntOp(i);
                            String[] paramParsing = action.substring(0, action.length() - 1).split(" ");
                            String[] parameters = Arrays.copyOfRange(paramParsing, 1, paramParsing.length);
                            for (int j = 0; j < grAction.getInstantiations().length; j++) {
                                grAction.setValueOfParameter(j, constants.indexOf(parameters[j]));
                            }
                            grAction.setDuration(duration);
                            pl.addActionToPlan(time, grAction);
                        }
                    }
                }
            }
            fr.close();
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void reformatPlan(String plan, int lastPlanningTime) {
        File file = new File("problem" + threadNumber + plan);
        TreeMap<Double, Set<String>> actionMap = new TreeMap<>();
        try {
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);  //creates a buffering character input stream
            String line;
            while ((line = br.readLine()) != null) {
                int separator = line.indexOf(":");
                addAction(actionMap, Double.parseDouble(line.substring(0, separator)), line.substring(separator + 2));
            }
            fr.close();
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        File output = new File("problem" + threadNumber + "/planning" + lastPlanningTime + "/plan");
        try {
            FileWriter fr = new FileWriter(output, false);
            Set<Double> keys = actionMap.keySet();
            Iterator<Double> itk = keys.iterator();
            while (itk.hasNext()) {
                Double k = itk.next();
                Set<String> ita = actionMap.get(k);
                Iterator<String> acs = ita.iterator();
                while (acs.hasNext()) {
                    String buff = acs.next();
                    fr.write(k + ": " + buff + "\n");
                }
            }
            fr.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }


    }

    public void addAction(TreeMap<Double, Set<String>> actionMap, Double key, String action) {
        Set<String> set = actionMap.get(key);
        if (set == null) {
            set = new HashSet<String>();
        }
        set.add(action);
        actionMap.put(key, set);
    }

    private RcllCodedProblem callExtPlanner() {

        //count the planning time

        //PARSING PHASE
        final StringBuilder strb = new StringBuilder();
        Parser parser = new Parser();
        try {
            String domainFile = "problem" + threadNumber + "/" + domainS;
            String problemFile = problemS + threadNumber + "/" + problemS + threadNumber + ".pddl";
            parser.parse(domainFile, problemFile);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        log.warn("parsed\n");

        //ENCODING PHASE

        final Domain domain = parser.getDomain();
        final Problem problem = parser.getProblem();

        pl = new Planner(domainS, problemS + threadNumber + ".pddl", cp, plannerBinary);

        try {
            RcllCodedProblem cp = Encoder.encode(domain, problem);
            
            //PLANNING PHASE

            pl.plan(threadNumber);

            return cp;

        } catch (IllegalArgumentException ilException) {
            log.error("the problem to encode is not ADL, \":requirements\" not supported at this time\n");
            return null;
        }
    }

    public String getPlanString() {
        return pl.planToString(cp);
    }



    public ImmutablePair<List<Order>, ImmutablePair<TemporalGraph, RcllCodedProblem>> getResult() {
        return new ImmutablePair<>(goals, new ImmutablePair<>(tgraph, cp));
    }

    private void generateProblemFile(List<Order> goals) {

        try {
            File file = new File("problem" + threadNumber + "/" + problemS + threadNumber + ".pddl");
            FileWriter fr = new FileWriter(file, false);
            fr.write("(define (problem task)\n" +
                    "(:domain rcll)\n" +
                    "(:objects\n");
            Map<String, Set<String>> objects = kb.getObjects();
            for (Map.Entry<String,Set<String>> entry : objects.entrySet()){
                if (entry.getValue().isEmpty()) {
                    continue;
                }
                
                for (String object : entry.getValue()){
                    fr.write(object + " ");
                }
                fr.write(" - " + entry.getKey() + "\n");
            }

            for (Order p : goals) {
                fr.write("p" + p.getId() + " ");
            }

            fr.write(" - product\n" +
                    ")\n" +
                    "(:init\n");


            kb.prettyPrintKBtoProblemFile(fr);

            generateGoalOnProblemFile(goals, fr);



            fr.write(")\n");
            fr.write("(:goal (and\n");
            for (Order p : goals) {
                fr.write("(delivered p" + p.getId() + ")\n");
            }
            fr.write(")\n" +
                    ")\n" +
                    "(:metric minimize  (total-time) )\n" +
                    "\n" +
                    ")");
            fr.close();

        } catch (IOException e) {
            log.error(e);
        }
    }

    //ATAI: given a set of goal chosen by the goalreasoner, model the related PDDL atoms here to be written in the problem file
    private void generateGoalOnProblemFile(List<Order> goals, FileWriter fr) throws IOException {
        for (Order p : goals) {
            String product_name = "p" + p.getId();
            ProductComplexity complexity = p.getComplexity();

            fr.write(new Atom("required_color_of_base",product_name + ";" + this.kb.baseToColorInPddl(p.getBase())).prettyPrinter());
            fr.write(new Atom("required_color_of_cap",product_name + ";" + this.kb.capToColorInPddl(p.getCap())).prettyPrinter());

            if (p.getDeliveryPeriodBegin() == 0){
                fr.write(new Atom("deliverable", product_name).prettyPrinter());
            }else{
                fr.write(new Atom("tocheck", product_name).prettyPrinter());
                fr.write(new Atom("=", "(deliveryWindowStart "+ product_name + ") " + p.getDeliveryPeriodBegin()).prettyPrinter());
            }

            if (complexity == ProductComplexity.C0){
                fr.write(new Atom("is_c0",product_name).prettyPrinter());
                continue;
            }
            
            fr.write(new Atom("required_color_of_first_ring",product_name+";"+this.kb.ringToColorInPddl(p.getRing1().getColor())).prettyPrinter());

            if (complexity == ProductComplexity.C1){
                fr.write(new Atom("is_c1",product_name).prettyPrinter());
                continue;
            }
            
            fr.write(new Atom("required_color_of_second_ring",product_name+";"+this.kb.ringToColorInPddl(p.getRing2().getColor())).prettyPrinter());

            if (complexity == ProductComplexity.C2){
                fr.write(new Atom("is_c2",product_name).prettyPrinter());
                continue;
            }

            fr.write(new Atom("required_color_of_third_ring",product_name+";"+this.kb.ringToColorInPddl(p.getRing3().getColor())).prettyPrinter());
            fr.write(new Atom("is_c3",product_name).prettyPrinter());
        }
    }

    //ATAI: implement the goalSelector here
    private List<Order> goalSelector() {
        List<Order> toPlan = new ArrayList<>();

        for (Order o : refboxClient.getAllOrders()){
            if (toPlan.size() > 2) {
                break;
            }
            if (o.getDeliveryPeriodEnd() >= 10*60){
                toPlan.add(o);
                log.info("Choosen Order: " + o.getId() + " End Time: " + o.getDeliveryPeriodEnd() );
            }
        }

        log.info(toPlan.size() + " orders choosen!");
        return toPlan;
    }


    public void writeDistance(FileWriter fr, String from, String fromCompleteName, MachineName to) throws IOException {
        String nameTo = to.getRawMachineName();
        if (to.isCapStation()) {
            if (!fromCompleteName.equals(nameTo + "_input"))
                writeDistanceCheck(fr, from, "cs" + nameTo.charAt(4) + "_input", distanceMatrix.get(fromCompleteName + nameTo + "_input"), false);
            if (!fromCompleteName.equals(nameTo + "_output"))
                writeDistanceCheck(fr, from, "cs" + nameTo.charAt(4) + "_output", distanceMatrix.get(fromCompleteName + nameTo + "_output"), false);
            if (!fromCompleteName.equals(nameTo + "_shelf"))
                writeDistanceCheck(fr, from, "cs" + nameTo.charAt(4) + "_shelf", distanceMatrix.get(fromCompleteName + nameTo + "_shelf"), true);
        } else if (to.isRingStation()) {
            if (!fromCompleteName.equals(nameTo + "_input"))
                writeDistanceCheck(fr, from, "rs" + nameTo.charAt(4) + "_input", distanceMatrix.get(fromCompleteName + nameTo + "_input"), false);
            if (!fromCompleteName.equals(nameTo + "_output"))
                writeDistanceCheck(fr, from, "rs" + nameTo.charAt(4) + "_output", distanceMatrix.get(fromCompleteName + nameTo + "_output"), false);
        } else if (to.isBaseStation() && !fromCompleteName.equals(nameTo + "_output")) {
            writeDistanceCheck(fr, from, nameTo.substring(2).toLowerCase(Locale.ROOT), distanceMatrix.get(fromCompleteName + nameTo + "_output"), false);
        } else if (to.isDeliveryStation() && !fromCompleteName.equals(nameTo + "_input")) {
            writeDistanceCheck(fr, from, nameTo.substring(2).toLowerCase(Locale.ROOT), distanceMatrix.get(fromCompleteName + nameTo + "_input"), false);
        }
    }

    public void writeDistanceCheck(FileWriter fr, String from, String to, double cost, boolean isBufferTask) throws IOException {
        int seconds = (int) (cost * timeFactor);
        if (!from.equals(to)) {
            if (isBufferTask)
                fr.write("( = (distanceBuffer " + from + " " + to + ") " + (seconds + productionTimes.getBufferTask()) + ")\n ");
            else
                fr.write("( = (distance " + from + " " + to + ") " + (seconds + productionTimes.getNormalTask()) + ")\n ");
            //System.out.print(" = (distance " + from + " " + to + ") " + cost + ")\n ");
        }
    }
}