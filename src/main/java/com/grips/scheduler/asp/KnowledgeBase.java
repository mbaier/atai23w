package com.grips.scheduler.asp;

import com.grips.persistence.domain.MachineInfoRefBox;
import com.grips.config.ProductionConfig;
import com.grips.persistence.dao.*;
import com.grips.persistence.domain.Atom;
import com.grips.persistence.domain.WorldKnowledge;
import com.rcll.domain.Order;
import com.rcll.domain.Base;
import com.rcll.domain.Cap;
import com.rcll.domain.ProductComplexity;
import com.rcll.domain.RingColor;
import com.rcll.planning.encoding.IntOp;
import com.rcll.planning.encoding.RcllCodedProblem;
import com.rcll.planning.util.TempAtom;
import com.rcll.refbox.RefboxClient;
import fr.uga.pddl4j.util.IntExp;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

@CommonsLog
@Service
public class KnowledgeBase {

    private final AtomDao atomDao;
    @Value("${gameconfig.planningparameters.numberofrobots}")
    private int numberRobots;
    private final MachineInfoRefBoxDao machineInfoRefBoxDao;
    private RcllCodedProblem cp;

    private Map<String,Set<String>> objects;

    @Autowired
    private ProductionConfig productionConfig;

    @Value("${gameconfig.productiontimes.normalTask}")
    private int normalTaskDuration;
    @Value("${gameconfig.productiontimes.bufferTask}")
    private int bufferTaskDuration;
    @Value("${gameconfig.productiontimes.moveTask}")
    private int moveTaskDuration;

    private final RefboxClient refboxClient;
    public  KnowledgeBase(AtomDao atomDao, MachineInfoRefBoxDao machineInfoRefBoxDao, ProductionConfig productionConfig,
                          RefboxClient refboxClient) {
        this.atomDao = atomDao;
        this.machineInfoRefBoxDao = machineInfoRefBoxDao;
        this.productionConfig = productionConfig;
        this.refboxClient = refboxClient;
    }


    private void initializeObjectsMap(){
        this.objects = new HashMap<String,Set<String>>();
        this.objects.put("location", new HashSet<String>());
        this.objects.put("side", new HashSet<String>());
        this.objects.put("robot", new HashSet<String>());
        this.objects.put("assembly", new HashSet<String>());
        this.objects.put("base", new HashSet<String>());
        this.objects.put("ring", new HashSet<String>());
        this.objects.put("cap", new HashSet<String>());
        this.objects.put("base_color", new HashSet<String>());
        this.objects.put("ring_color", new HashSet<String>());
        this.objects.put("cap_color", new HashSet<String>());

        this.objects.get("location").add("start");

        this.objects.get("side").add("input");
        this.objects.get("side").add("output");
        this.objects.get("side").add("slide");

        this.objects.get("base_color").add("base_red");
        this.objects.get("base_color").add("base_black");
        this.objects.get("base_color").add("base_silver");

        this.objects.get("ring_color").add("ring_blue");
        this.objects.get("ring_color").add("ring_green");
        this.objects.get("ring_color").add("ring_orange");
        this.objects.get("ring_color").add("ring_yellow");

        this.objects.get("cap_color").add("cap_grey");
        this.objects.get("cap_color").add("cap_black");
    }


    private void addBase(String color, String machine, String count){
        String red_base_name = "b_"+color+"_"+machine+"_"+count;
        this.objects.get("base").add(red_base_name);

        String red_assembly_name = "as_"+color+"_"+machine+"_"+count;
        this.objects.get("assembly").add(red_assembly_name);

        this.add(new Atom("has_color",red_base_name+";base_"+color));
        this.add(new Atom("mounted_base",red_base_name +";"+ red_assembly_name));
        this.add(new Atom("n_mounted_first_ring",red_assembly_name));
        this.add(new Atom("n_mounted_second_ring",red_assembly_name));
        this.add(new Atom("n_mounted_third_ring",red_assembly_name));
        this.add(new Atom("n_mounted_cap",red_assembly_name));
        this.add(new Atom("can_be_unloaded",red_assembly_name+";"+machine));
    }


    //ATAI: define the initial atoms of the KB here
    public void generateInitialKB () {
        initializeObjectsMap();

        this.add(new Atom("=","(color_costs ring_blue) " + this.refboxClient.getRingByColor(RingColor.Blue).getRawMaterial()));
        this.add(new Atom("=","(color_costs ring_green) " + this.refboxClient.getRingByColor(RingColor.Green).getRawMaterial()));
        this.add(new Atom("=","(color_costs ring_orange) " + this.refboxClient.getRingByColor(RingColor.Orange).getRawMaterial()));
        this.add(new Atom("=","(color_costs ring_yellow) " + this.refboxClient.getRingByColor(RingColor.Yellow).getRawMaterial()));

        // for(int i = 1; i<=this.numberRobots; i++){
        //     String robot = "robot_"+ i;
        //     this.objects.get("robot").add(robot);
        //     this.add(new Atom("at", robot + ";start;output"));
        //     this.add(new Atom("free", robot));
        //     this.add(new Atom("n_robot_holding", robot));
        // }

        String robot = "robot_1";
        this.objects.get("robot").add(robot);
        this.add(new Atom("at", robot + ";start;output"));
        this.add(new Atom("free", robot));
        this.add(new Atom("n_robot_holding", robot));

        this.add(new Atom("is_input","input"));
        this.add(new Atom("is_output","output"));
        this.add(new Atom("is_slide","slide"));

        String grey_cap_machine = "C-"+this.productionConfig.getGrey_cap_machine();
        String black_cap_machine = "C-"+this.productionConfig.getBlack_cap_machine();

        for (MachineInfoRefBox machine : this.machineInfoRefBoxDao.findByTeamColor(this.refboxClient.getTeamColor().get())){
            if (machine.getType().equals("SS")){
                continue;
            }
            String machine_name = machine.getName();
            if (machine_name.equals(grey_cap_machine)){
                machine_name = machine_name + "_grey";
            }
            if (machine_name.equals(black_cap_machine)){
                machine_name = machine_name + "_black";
            }
            this.objects.get("location").add(machine_name);

            this.add(new Atom("empty",machine_name+";input"));
            this.add(new Atom("empty",machine_name+";output"));
            this.add(new Atom("on_idle",machine_name));

            if(machine.getType().equals("BS")){
                this.add(new Atom("is_base_location",machine_name));
                this.add(new Atom("=","(machine_time " + machine_name + ");"+ this.normalTaskDuration));

                for(int i = 0; i<this.productionConfig.getMaxSimultaneousProducts();i++) {
                    this.addBase("red",machine_name,""+i);
                    this.addBase("black",machine_name,""+i);
                }
            }

            if (machine.getType().equals("RS")){
                this.add(new Atom("is_ring_location",machine_name));
                this.add(new Atom("empty",machine_name+";slide"));
                this.add(new Atom("can_station_be_loaded",machine_name));
                this.add(new Atom("=","(bases_on_slide "+machine_name+");0"));
                this.add(new Atom("=","(machine_time "+machine_name+");"+ this.normalTaskDuration));
                
                for(int i = 0; i<this.productionConfig.getMaxSimultaneousProducts();i++){
                    String ring1 = "r1_"+machine_name+"_"+i;
                    this.objects.get("ring").add(ring1);

                    this.add(new Atom("has_color",ring1+";"+ringToColorInPddl(machine.getRing1())));
                    this.add(new Atom("stocked_component", machine_name+";"+ring1));


                    String ring2 = "r2_"+machine_name+"_"+i;
                    this.objects.get("ring").add(ring2);

                    this.add(new Atom("has_color",ring2+";"+ringToColorInPddl(machine.getRing2())));
                    this.add(new Atom("stocked_component", machine_name+";"+ring2));
                }
            }

            if (machine.getType().equals("CS")){
                this.add(new Atom("is_cap_location",machine_name));
                this.add(new Atom("nothing_bufferd",machine_name));
                this.add(new Atom("can_station_be_loaded",machine_name));
                this.add(new Atom("=","(machine_time "+machine_name+");"+ this.bufferTaskDuration));
                for(int i = 0; i<this.productionConfig.getMaxSimultaneousProductsSameCap(); i++){
                    String base_name = "bg_"+machine_name+"_"+i;
                    this.objects.get("base").add(base_name);
                    String cap_name = "cp_"+machine_name+"_"+i;
                    this.objects.get("cap").add(cap_name);
                    String assembly_name = "as_"+machine_name+"_"+i;
                    this.objects.get("assembly").add(assembly_name);

                    this.add(new Atom("has_color",base_name+";base_silver"));
                    if (machine_name.equals(grey_cap_machine + "_grey")){
                        this.add(new Atom("has_color",cap_name+";cap_grey"));
                    } else if (machine_name.equals(black_cap_machine + "_black")){
                        this.add(new Atom("has_color",cap_name+";cap_black"));
                    }

                    this.add(new Atom("mounted_base",base_name+";"+assembly_name));
                    this.add(new Atom("mounted_cap",cap_name+";"+assembly_name));
                    this.add(new Atom("n_mounted_first_ring",assembly_name));
                    this.add(new Atom("n_mounted_second_ring",assembly_name));
                    this.add(new Atom("n_mounted_third_ring",assembly_name));
                    this.add(new Atom("stocked_assembly",machine_name+";"+assembly_name));
                }
            }

            if(machine.getType().equals("DS")){
                this.add(new Atom("is_delivery_location",machine_name));  
                this.add(new Atom("=","(machine_time "+machine_name+");"+ this.normalTaskDuration));
            }
        }
    }

    public void generateDistances (Map<String,Double> distanceMap) {
        for (String location : this.objects.get("location")){
            for (String location_two : this.objects.get("location")){
                if (location_two.equals("start")){
                    continue;
                }
                String machine1 = "";
                machine1 = location.replace("_grey", "");
                machine1 = machine1.replace("_black", "");
                
                String machine2 = "";
                machine2 = location_two.replace("_grey", "");
                machine2 = machine2.replace("_black", "");

                for (String side1 : this.objects.get("side")){
                    for (String side2 : this.objects.get("side")){
                        if (location.equals(location_two) && side1.equals(side2)) {
                            continue;
                        }
                        Double distance = distanceMap.get((machine1+"_"+side1+machine2+"_"+side2).replace("slide", "output"));
                        if(distance == null){
                            this.add(new Atom("=","(distance " + location + " " + location_two + " " + side1 + " " + side2 + " ) " + this.moveTaskDuration));
                        }else {
                            this.add(new Atom("=","(distance " + location + " " + location_two + " " + side1 + " " + side2 + " ) " + distance.intValue()));
                        }
                    }
                }
            }
        }
    }

    public String ringToColorInPddl(RingColor ringColor) {
        switch (ringColor) {
            case Blue:
                return "ring_blue";
            case Green:
                return "ring_green";
            case Orange:
                return "ring_orange";
            case Yellow:
                return "ring_yellow";
        }
        throw new IllegalArgumentException("Unkown ring color: " + ringColor);
    }

    public String baseToColorInPddl(Base baseColor){
        switch (baseColor) {
            case Silver:
                return "base_silver";
            case Black:
                return "base_black";
            case Red:
                return "base_red";
        }
        throw new IllegalArgumentException("Unkown base color: " + baseColor);
    }

    public String capToColorInPddl(Cap capColor){
        switch (capColor) {
            case Black:
                return "cap_black";
            case Grey:
                return "cap_grey";
        }
        throw new IllegalArgumentException("Unkown cap color: " + capColor);
    }

    //ATAI: given a set of goal chosen by the goalreasoner, model the related KB atoms here
    public void generateOrderKb (List<Order> goals) {
        for (Order p : goals) {
            String product_name = "p" + p.getId();
            ProductComplexity complexity = p.getComplexity();

            this.add(new Atom("required_color_of_base",product_name + ";" + this.baseToColorInPddl(p.getBase())));
            this.add(new Atom("required_color_of_cap",product_name + ";" + this.capToColorInPddl(p.getCap())));

            if (p.getDeliveryPeriodBegin() == 0){
                this.add(new Atom("deliverable", product_name));
            }else{
                this.add(new Atom("tocheck", product_name));
                this.add(new Atom("=", "(deliveryWindowStart "+ product_name + ") " + p.getDeliveryPeriodBegin()));
            }

            if (complexity == ProductComplexity.C0){
                this.add(new Atom("is_c0",product_name));
                continue;
            }
            
            this.add(new Atom("required_color_of_first_ring",product_name+";"+this.ringToColorInPddl(p.getRing1().getColor())));

            if (complexity == ProductComplexity.C1){
                this.add(new Atom("is_c1",product_name));
                continue;
            }
            
            this.add(new Atom("required_color_of_second_ring",product_name+";"+this.ringToColorInPddl(p.getRing2().getColor())));

            if (complexity == ProductComplexity.C2){
                this.add(new Atom("is_c2",product_name));
                continue;
            }

            this.add(new Atom("required_color_of_third_ring",product_name+";"+this.ringToColorInPddl(p.getRing3().getColor())));
            this.add(new Atom("is_c3",product_name));
        }
    }


    public void printKB () {
        for( Atom at : atomDao.findAll())
            System.out.println(at.toString());
    }

    public Map<String,Set<String>> getObjects(){
        return this.objects;
    }

    public void prettyPrintKBtoProblemFile (FileWriter fr) throws IOException {
        for (Atom at : atomDao.findAll())
            if(!at.getName().equals("holdafter") )
                fr.write(at.prettyPrinter());
    }


    public void add (Atom at) {
        if (atomDao.findByNameAndAttributes(at.getName(), at.getAttributes()).size() == 0 )
            atomDao.save(at);
        else
            log.warn("Trying to add to KB atom " + at.toString() + " already present");

    }



    public boolean isNotInKB (String pd, String attributes) {
        return atomDao.findByNameAndAttributes(pd, attributes).isEmpty();
    }

    public void setCp (RcllCodedProblem cp) {
        this.cp = cp;
    }


    public String intToString (int number) {
        switch (number) {

            case 0:
                return "zero";

            case 1:
                return "one";

            case 2:
                return "two";

            case 3:
                return "three";

            default:
                return null;

        }
    }


    public void purgeKB () {
        atomDao.deleteAll();
    }







    /**
     * check if end precondition of action are satisfied
     * @param action
     * @return
     */



}
