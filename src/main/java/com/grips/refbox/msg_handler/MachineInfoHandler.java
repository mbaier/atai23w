package com.grips.refbox.msg_handler;

import com.grips.persistence.dao.MachineInfoRefBoxDao;
import com.grips.persistence.domain.ExplorationZone;
import com.grips.persistence.domain.MachineInfoRefBox;
import com.grips.refbox.PrivateRefBoxHandler;
import com.grips.refbox.PublicRefBoxHandler;
import com.grips.scheduler.GameField;
import com.grips.scheduler.production.SubProductionTaskUpdater;
import com.grips.tools.PathEstimator;
import com.rcll.domain.MachineName;
import com.rcll.domain.TeamColor;
import com.rcll.refbox.RefboxClient;
import lombok.Synchronized;
import lombok.extern.apachecommons.CommonsLog;
import org.dozer.Mapper;
import org.robocup_logistics.llsf_msgs.MachineInfoProtos;
import org.robocup_logistics.llsf_msgs.TeamProtos;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

@CommonsLog
@Service
public class MachineInfoHandler implements Consumer<MachineInfoProtos.MachineInfo> {

    private final RefboxClient refboxClient;
    private final SubProductionTaskUpdater subProductionTaskUpdater;
    private final MachineInfoRefBoxDao machineInfoRefBoxDao;
    private final Mapper mapper;
    private final GameField gameField;
    private final PathEstimator pathEstimator;

    public MachineInfoHandler(RefboxClient refboxClient,
                              SubProductionTaskUpdater subProductionTaskUpdater,
                              MachineInfoRefBoxDao machineInfoRefBoxDao,
                              Mapper mapper, GameField gameField,
                              PathEstimator pathEstimator,
                              PrivateRefBoxHandler privateRefBoxHandler,
                              PublicRefBoxHandler publicRefBoxHandler) {
        this.refboxClient = refboxClient;
        this.subProductionTaskUpdater = subProductionTaskUpdater;
        this.machineInfoRefBoxDao = machineInfoRefBoxDao;
        this.mapper = mapper;
        this.gameField = gameField;
        this.pathEstimator = pathEstimator;
        privateRefBoxHandler.setMachineInfoCallback(this); //todo check if public of private peer!!
        publicRefBoxHandler.setMachineInfoCallback(this); //todo check if public of private peer!!
        this.machineInfoRefBoxDao.deleteAll(); //delete all at start of game!!
    }

    @Override
    @Synchronized
    public void accept(MachineInfoProtos.MachineInfo machineInfo) {
        refboxClient.updateMachineStates(machineInfo);
        subProductionTaskUpdater.updateTaskPerMachine(machineInfo);
        List<MachineInfoRefBox> machines = new ArrayList<>();
        for (MachineInfoProtos.Machine tmp_machine : machineInfo.getMachinesList()) {
            if (!tmp_machine.hasZone()) {
                log.warn("Machine [" + tmp_machine.getName() + "] has no Zone, skipping...");
                continue;
            }
            MachineInfoRefBox m_info = mapper.map(tmp_machine, MachineInfoRefBox.class);
            MachineInfoRefBox prevMachineState = machineInfoRefBoxDao.findByName(m_info.getName());
            String prevState = "";
            if (prevMachineState != null) {
                prevState = prevMachineState.getState();
            }

            if (!prevState.equalsIgnoreCase(m_info.getState())) {
                if ("BROKEN".equalsIgnoreCase(m_info.getState())) {
                    //todo what to do in that case?!?!?!
                    log.warn("Machine broken: " + m_info.getName());
                }
            }
            machineInfoRefBoxDao.save(m_info);
            // duplicate machine for robot updates
            MachineInfoRefBox m_info_mirrored = new MachineInfoRefBox();
            ExplorationZone zone = gameField.getZoneByName(m_info.getZone());

            if (zone == null) {
                log.error("Dropping machine info for: " + m_info.getName() + " because zone: " + m_info.getZone() + " not in gamefield!!!");
                return;
            }

            ExplorationZone mirrored_zone = zone.getMirroredZone();
            m_info_mirrored.setZone(mirrored_zone.getZoneName());
            String machineName = m_info.getName();
            if (machineName.startsWith("C-")) {
                machineName = machineName.replaceFirst("C-", "M-");
            } else {
                machineName = machineName.replaceFirst("M-", "C-");
            }
            m_info_mirrored.setName(machineName);

            long zoneNumber = gameField.getZoneByName(m_info.getZone()).getZoneNumber();
            int xPos = (int) (zoneNumber / 10L);
            int yPos = (int) (zoneNumber % 10);
            int rotation = gameField.rotateMachineIfNecessary(xPos, yPos, new MachineName(m_info.getName()), m_info.getRotation());
            m_info_mirrored.setRotation(rotation);
            m_info_mirrored.setRing1(m_info.getRing1());
            m_info_mirrored.setRing2(m_info.getRing2());
            m_info_mirrored.setCorrectlyReported(m_info.getCorrectlyReported());
            m_info_mirrored.setExplorationTypeState(m_info.getExplorationTypeState());
            m_info_mirrored.setExplorationZoneState(m_info.getExplorationTypeState());
            m_info_mirrored.setState(m_info.getState());
            m_info_mirrored.setTeamColor(machineName.startsWith("C-") ? TeamColor.CYAN : TeamColor.MAGENTA);
            m_info_mirrored.setType(m_info.getType());

            machines.add(m_info);
            machines.add(m_info_mirrored);
            machineInfoRefBoxDao.save(m_info_mirrored);

            try {
                pathEstimator.addMachine(m_info.getName(), zone.getZoneCenter(), m_info.getRotation());
                //todo does not work! needed in real game!
                //maybe needs to be commented out for C0 challange!
                pathEstimator.addMachine(m_info_mirrored.getName(), mirrored_zone.getZoneCenter(), m_info_mirrored.getRotation());
            } catch (Exception exception) {
                log.error("Error on adding to pathEstimator: " + m_info.toString(), exception);
            }

            //System.out.println("\n\n\n");
            //double l1 = pathEstimator.pathLength("M-CS1", SubProductionTask.MachineSide.OUTPUT, "M-RS1", SubProductionTask.MachineSide.INPUT);
            //double l2 = pathEstimator.pathLength("M-CS1", SubProductionTask.MachineSide.OUTPUT, "M-RS2", SubProductionTask.MachineSide.INPUT);
            //System.out.println("Pathlenghts: RS1: " + l1 + " RS2: " + l2);

            if (m_info.getState().equalsIgnoreCase("BROKEN")) {
                // a machine is broken, we need to fail any associated task
                // productionScheduler.machineBroken(m_info.getName()); todo may add to IScheduler?
            }
        }
    }
}
