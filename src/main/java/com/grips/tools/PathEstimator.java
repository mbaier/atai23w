package com.grips.tools;

import com.grips.config.GameFieldConfig;
import com.rcll.domain.MachineSide;
import com.rcll.refbox.RefboxClient;
import com.shared.domain.Point2d;
import com.shared.domain.Point2dInt;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@CommonsLog
public class PathEstimator {
    private Grid _grid;
    private AStarFinder _astarFinder;
    private Map<String, GridMPS> _machines;
    private boolean shortestDisposalCalculated = false;

    private String disposalCS1 = null;
    private String disposalCS2 = null;

    private HashMap<String, Double> rotStartMatrix;
    private HashMap<String, Double> rotEndMatrix;

    private final GameFieldConfig gameFieldConfig;
    private final RefboxClient refboxClient;

    public PathEstimator(RefboxClient refboxClient,
                         GameFieldConfig gameFieldConfig) {
        this.gameFieldConfig = gameFieldConfig;
        this.refboxClient = refboxClient;
       // create grid representing our gamefield, one node is 10cm x 10cm
       _grid = new Grid(gameFieldConfig.getWidth()*10, gameFieldConfig.getHeight()*10);
       _astarFinder = new AStarFinder(true, true);
       _machines = new HashMap<>();
        rotStartMatrix = new HashMap<String, Double>();
        rotEndMatrix = new HashMap<String, Double>();
    }

    public Point2dInt rcllToGrid(Point2d rcll) {
        return new Point2dInt(
                (int)((rcll.getX() + gameFieldConfig.getWidth() / 2) * 10),
                (int)((rcll.getY() + 0) * 10));
    }
    public void addMachine(String name, Point2d centerMachine, int rotation) {

        if (!_machines.containsKey(name)) {
            // account for negative X values
            Point2dInt centerGrid = this.rcllToGrid(centerMachine);
            log.info("Adding machine " + name + "( " + centerGrid +" )");
            List<Node> occupiedNodes = new ArrayList<>();
            for (int i = 1; i <= 10; ++i) {
                for (int j = 1; j <= 10; ++j) {
                    Point2dInt toMark = new Point2dInt(centerGrid.getX() + i, centerGrid.getY() + j);
                    _grid.setOccupied(toMark, true);
                    occupiedNodes.add(_grid.getNodeAt(toMark));
                }
            }

            int inputX, inputY, outputX, outputY;

            //not +10 but i think +5 is sufficient? since center grid is 55!
            switch (rotation) {
                case 0: inputX = centerGrid.getX() + 10; inputY = centerGrid.getY() + 5; outputX = centerGrid.getX() - 1; outputY = centerGrid.getY() + 5; break;
                case 45: inputX = centerGrid.getX() + 10; inputY = centerGrid.getY() - 1; outputX = centerGrid.getX() - 1; outputY = centerGrid.getY() + 10; break;
                case 90: inputX = centerGrid.getX() + 5; inputY = centerGrid.getY() - 1; outputX = centerGrid.getX() + 5; outputY = centerGrid.getY() + 10; break;
                case 135: inputX = centerGrid.getX() - 1; inputY = centerGrid.getY() - 1; outputX = centerGrid.getX() + 10; outputY = centerGrid.getY() + 10; break;
                case 180: inputX = centerGrid.getX() - 1; inputY = centerGrid.getY() + 5; outputX = centerGrid.getX() + 10; outputY = centerGrid.getY() + 5; break;
                case 225: inputX = centerGrid.getX() - 1; inputY = centerGrid.getY() + 10; outputX = centerGrid.getX() + 10; outputY = centerGrid.getY() - 1; break;
                case 270: inputX = centerGrid.getX() + 5; inputY = centerGrid.getY() + 10; outputX = centerGrid.getX() + 5; outputY = centerGrid.getY() - 1; break;
                case 315: inputX = centerGrid.getX() + 10; inputY = centerGrid.getY() + 10; outputX = centerGrid.getX() - 1; outputY = centerGrid.getY() - 1; break;
                default: inputX = centerGrid.getX(); inputY = centerGrid.getY(); outputX = centerGrid.getX(); outputY = centerGrid.getY();
                    System.err.println("Rotation of " + rotation + " not known to pathestimator!");
            }
            //throws error if DS is at corder (Output is not reachale);
            try {
                Node inputNode = _grid.getNodeAt(inputX, inputY); // we need to add 1, since we start at (1,1) instead of (0,0)
                Node outputNode = _grid.getNodeAt(outputX, outputY);

                GridMPS gmps = new GridMPS(inputNode, outputNode, occupiedNodes);
                _machines.put(name, gmps);
            } catch (Exception e) {
                log.error("Error on PathEstimator: ", e);
                log.error("INFO: " + centerGrid.toString());
                throw e;
            }


        }
    }

    public double pathLength(double startX, double startY, double endX, double endY) {
        _grid.resetPathCosts();

        //account for negative X values
        startX += gameFieldConfig.getWidth()/2.0;
        endX += gameFieldConfig.getWidth()/2.0;

        //also account for inverted Y
        startY = gameFieldConfig.getHeight() - startY;
        endY = gameFieldConfig.getHeight() - endY;

        // round positions to nearest integer to get start and end
        LinkedList<Node> path = _astarFinder.findPath((int) Math.round(startX), (int) Math.round(startY), (int) Math.round(endX), (int) Math.round(endY), _grid);
        if (path == null || path.size() == 0) {
            System.err.println("No path could be found from " + startX + "/" + startY + " to " + endX + "/" + endY);
            log.warn("No path found, returning 60!");
            return 60;
            //return Double.MAX_VALUE;
        } else {
            return _astarFinder.pathLength(path);
        }
    }

    public double pathLength(Point2dInt start, String endMachine, MachineSide endSide) {
        _grid.resetPathCosts();
        int startX = start.getX();
        int startY = start.getY();

        // round positions to nearest integer to get start and end
        Node endMachineNode = getMachineNode(endMachine, endSide);
        endMachineNode = _grid.getNeighbors(endMachineNode, true).stream()
                .findFirst()
                .orElseThrow();
        if (endMachineNode == null) {
            log.warn("No path found, returning 60!");
            return 60;
            //return Double.MAX_VALUE;
        }

        LinkedList<Node> path = _astarFinder.findPath(startX, startY, endMachineNode.getX(), endMachineNode.getY(), _grid);
        if (path == null || path.size() == 0) {
            log.error("No path could be found from " + startX + "/" + startY + " to " + endMachine + " " + endSide);
            //return Double.MAX_VALUE;
            return 60;
        } else {
            return _astarFinder.pathLength(path);
        }
    }

    public double pathLength(String startMachine, MachineSide startSide, String endMachine, MachineSide endSide) {
        _grid.resetPathCosts();

        Node endMachineNode = getMachineNode(endMachine, endSide);
        Node startMachineNode = getMachineNode(startMachine, startSide);
        if (startMachineNode == null || endMachineNode == null) {
            log.warn("No path found, returning 60!");
            return 60;
            //return Double.MAX_VALUE;
        }
        try {
            startMachineNode = _grid.getNeighbors(startMachineNode, true).stream().findFirst().orElseThrow();
            endMachineNode = _grid.getNeighbors(endMachineNode, true).stream().findFirst().orElseThrow();
        } catch (Exception ex) {
            log.info("Invalid path calculation: " + startMachine + "-" + startSide + " to " + endMachine + "-" + endSide);
            return 60; //todo fix this!!!
            //throw  ex;
        }
        LinkedList<Node> path = _astarFinder.findPath(startMachineNode.getX(), startMachineNode.getY(), endMachineNode.getX(), endMachineNode.getY(), _grid);
        if (path == null || path.size() == 0) {
            System.err.println("No path could be found from " + startMachine + " " + startSide + " to " + endMachine + " " + endSide + ". Setting it to default 40");
            return 40;
        } else {
            //_grid.printGridWithPath(path);
            Node start_oppositeSide;
            if (startSide == MachineSide.Input)
                start_oppositeSide = getMachineNode(startMachine, MachineSide.Output);
            else
                start_oppositeSide = getMachineNode(startMachine, MachineSide.Input);
            Node end_oppositeSide;
            if (endSide == MachineSide.Input)
                end_oppositeSide = getMachineNode(endMachine, MachineSide.Output);
            else
                end_oppositeSide = getMachineNode(endMachine, MachineSide.Input);
            if (path.size() > 1){
                double start_rotation_cost = rotationCost(path.get(path.size()-1), path.get(path.size()-2), start_oppositeSide, true);
                double end_rotation_cost = rotationCost(path.get(1), path.get(0) , end_oppositeSide, false);

                double start_rotation_degree = rotationDegree(path.get(path.size()-1), path.get(path.size()-2), start_oppositeSide);
                double end_rotation_degree = rotationDegree(path.get(1), path.get(0) , end_oppositeSide);


                //double start_rot_degree = getRotationDegree(path.get(path.size()-1), path.get(path.size()-2), start_oppositeSide);

                /*
                int counter = 0;
                while (counter < path.size()) {
                    System.out.println("++ (" + path.get(counter).getX() + "," + path.get(counter).getY() + ")");
                    counter++;
                }*/
                System.out.println("+++++++++++STARTING ROTATION BETWEEN " + startMachine + startSide + " AND " + endMachine + endSide + " IS " + start_rotation_degree + " WITH COST " + start_rotation_cost);
                System.out.println("+++++++++++ENDING ROTATION BETWEEN " + startMachine + startSide + " AND " + endMachine + endSide + " IS " + end_rotation_degree + " WITH COST " + end_rotation_cost);
                //System.out.println("FIRST NODE AND MACHINE SIDE (" + path.get(0).getX() + "," + path.get(0).getY() + ") (" + endMachineNode.getX() + "," + endMachineNode.getY() + ")"  );
                //System.out.println("++++++++++COORDINATE (" + path.get(path.size()-1).getX() + "," + path.get(path.size()-1).getY() + ") ("  + path.get(path.size()-2).getX() + "," + path.get(path.size()-2).getY() + ") ("  + oppositeSide.getX() + "," + oppositeSide.getY() + ")" + "("  + startMachineNode.getX() + "," + startMachineNode.getY() + ")" );
                rotStartMatrix.put(startMachine+"_"+startSide.toString().toLowerCase(Locale.ROOT)+endMachine+"_"+endSide.toString().toLowerCase(Locale.ROOT),start_rotation_degree);
                rotEndMatrix.put(startMachine+"_"+startSide.toString().toLowerCase(Locale.ROOT)+endMachine+"_"+endSide.toString().toLowerCase(Locale.ROOT),end_rotation_degree);

                return _astarFinder.pathLength(path) + start_rotation_cost + end_rotation_cost  ;


            }
            return _astarFinder.pathLength(path);  //+ rotation_cost;
        }
    }


    public double getRotationCost (String key, boolean isStarting) {
        if (isStarting)
            return rotStartMatrix.get(key);
        else
            return rotEndMatrix.get(key);
    }

    //to determine the initial orientation of the robot, I need robot position and the position of the opposite side of the machine. Then I calculate the vertex wrt the next point of the A* path
    public double rotationDegree (Node robot, Node direction, Node machine) {
        double angleDegree;
        try {
            double angleRad = Math.atan2(machine.getY() - robot.getY(), machine.getX() - robot.getX()) - Math.atan2(direction.getY() - robot.getY(), direction.getX() - robot.getX());
            angleDegree = Math.toDegrees(angleRad);
        } catch (Exception e) {
            angleDegree = 90;
            System.out.println("atan2 error, setting standard 90 value to angleRad: " + e);
        }
        if (angleDegree > 180)
            angleDegree = 360 - angleDegree;
        if (angleDegree < -180)
            angleDegree = -360 - angleDegree;
        return angleDegree;
    }

    public double rotationCost (Node robot, Node direction, Node machine, boolean isStart) {
        double rot_degree = Math.abs(rotationDegree(robot, direction, machine));
        if (isStart) {  //the robot starts always with its back facing the machine
            if (rot_degree < 180)
                rot_degree += 180;
            else
                rot_degree -= 180;
        }
        double rotation_cost;
        if (rot_degree < 10)
            rotation_cost = 0;
        else if (rot_degree < 50)
            rotation_cost = 7;
        else if (rot_degree <= 90)
            rotation_cost = 14;
        else if (rot_degree <= 135)
            rotation_cost = 21;
        else rotation_cost = 28;
        return rotation_cost;
    }

    public Node getMachineNode(String machine, MachineSide side) {
        if (!_machines.containsKey(machine)) {
            log.error("Machine " + machine + " not known to pathestimator!");
            return null;
        }

        Node machineNode;
        if (side == MachineSide.Input || side == MachineSide.Shelf) {
            machineNode = _machines.get(machine).getInput();
        } else if (side == MachineSide.Output) {
            machineNode = _machines.get(machine).getOutput();
        } else {
            log.error("Unknown machine side specified for pathestimator on machine " + machine);
            return null;
        }
        return machineNode;
    }

    public void printCurrentGrid() {
        _grid.printGridWithPath(new LinkedList<>());
    }

    public boolean allMachinesAvailable() {
        if (_machines.size() == 14) {
            return true;
        }

        return false;
    }

    public int availableMachines() {
        return _machines.size();
    }

    public void calculateShortestDisposalMachines() {
        if (!allMachinesAvailable() || shortestDisposalCalculated) return;

        String colorprefix = refboxClient.isCyan() ? "C-" : "M-";
        String cs1 = colorprefix + "CS1";
        String cs2 = colorprefix + "CS2";
        String rs1 = colorprefix + "RS1";
        String rs2 = colorprefix + "RS2";

        double c1rs1 = pathLength(cs1, MachineSide.Input, rs1, MachineSide.Input);
        double c1rs2 = pathLength(cs1, MachineSide.Input, rs2, MachineSide.Input);
        double c2rs1 = pathLength(cs2, MachineSide.Input, rs1, MachineSide.Input);
        double c2rs2 = pathLength(cs2, MachineSide.Input, rs2, MachineSide.Input);

        // no find the shortest combined path
        if ((c1rs1 + c2rs2) < (c1rs2 + c2rs1)) {
            disposalCS1 = rs1;
            disposalCS2 = rs2;
        } else {
            disposalCS1 = rs2;
            disposalCS2 = rs1;
        }

        System.out.println("Defined preferred disposal for CS1 as " + disposalCS1);
        System.out.println("Defined preferred disposal for CS2 as " + disposalCS2);
        shortestDisposalCalculated = true;
    }

    public String getDisposalCS1() {
        return disposalCS1;
    }

    public String getDisposalCS2() {
        return disposalCS2;
    }
}
