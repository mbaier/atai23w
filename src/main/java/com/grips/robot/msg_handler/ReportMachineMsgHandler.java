package com.grips.robot.msg_handler;

import com.rcll.domain.MachineName;
import com.grips.persistence.dao.MachineReportDao;
import com.grips.persistence.domain.MachineReport;
import com.rcll.domain.ZoneName;
import com.rcll.refbox.RefboxClient;
import com.grips.scheduler.GameField;
import com.grips.scheduler.exploration.MachineExplorationService;
import com.grips.tools.DiscretizeAngles;
import lombok.extern.apachecommons.CommonsLog;
import org.dozer.Mapper;
import org.robocup_logistics.llsf_msgs.GripsExplorationReportMachineProtos;
import org.robocup_logistics.llsf_msgs.MachineReportProtos;
import org.springframework.stereotype.Service;

import java.util.function.Consumer;

@Service
@CommonsLog
public class ReportMachineMsgHandler implements Consumer<GripsExplorationReportMachineProtos.GripsExplorationReportMachine> {

    private final Mapper mapper;
    private final MachineExplorationService explorationService;
    private final MachineReportDao machineReportDao;

    private final RefboxClient refboxClient;

    public ReportMachineMsgHandler(Mapper mapper,
                                   MachineExplorationService explorationService,
                                   MachineReportDao machineReportDao,
                                   RefboxClient refboxClient) {
        this.mapper = mapper;
        this.explorationService = explorationService;
        this.machineReportDao = machineReportDao;
        this.refboxClient = refboxClient;
    }

    @Override
    public void accept(GripsExplorationReportMachineProtos.GripsExplorationReportMachine reportMachine) {
        log.info("Got report for maschine: "  + reportMachine);
        MachineReportProtos.MachineReportEntry entry = getReportInTeamColor(reportMachine);
        explorationService.markMachineAsReported(new MachineName(entry.getName()), new ZoneName(entry.getZone().name()));
        MachineReport machineReport = mapper.map(entry, MachineReport.class);
        log.info("Reporting: " + entry);
        machineReportDao.save(machineReport);


        this.refboxClient.sendReportMachine(
                new MachineName(machineReport.getName().toString()),
                new ZoneName(machineReport.getZone().toString()),
                machineReport.getRotation());
    }

    public MachineReportProtos.MachineReportEntry getReportInTeamColor(GripsExplorationReportMachineProtos.GripsExplorationReportMachine reportMachine) {
        log.info("Got report: " + reportMachine.toString());
        MachineName name = new MachineName(reportMachine.getMachineId());
        int rotation = (int) reportMachine.getOrientation();
        rotation = (rotation % 360 + 360) % 360;
        rotation = DiscretizeAngles.discretizeAngles(rotation);
        ZoneName zone = new ZoneName(reportMachine.getZone().toString());
        if (refboxClient.isCyan() && name.isMagenta() || refboxClient.isMagenta() && name.isCyan()) {
            name = name.mirror();
            rotation = GameField.rotateMachineIfNecessary((int) reportMachine.getZone().getNumber() / 10,
                    (int) reportMachine.getZone().getNumber() % 10, name, rotation);
            zone = zone.mirror();
        }
        MachineReportProtos.MachineReportEntry entry = MachineReportProtos.MachineReportEntry.newBuilder()
                .setName(name.getRawMachineName())
                .setZone(zone.toProto())
                .setRotation(rotation).build();
        return entry;
    }
}
