package com.grips.robot.msg_handler;

import com.grips.config.RefboxConfig;
import com.grips.config.StartPositionsConfig;
import com.grips.config.StationsConfig;
import com.grips.config.TeamColor;
import com.grips.scheduler.asp.AspScheduler;
import com.rcll.domain.GamePhase;
import com.rcll.domain.GameState;
import com.rcll.domain.Peer;
import com.grips.persistence.dao.BeaconSignalFromRobotDao;
import com.grips.persistence.domain.BeaconSignalFromRobot;
import com.rcll.domain.PeerState;
import com.rcll.llsf_comm.Key;
import com.rcll.protobuf_lib.RobotConnections;
import com.rcll.protobuf_lib.RobotMessageRegister;
import com.rcll.refbox.RefboxClient;
import com.grips.scheduler.api.DbService;
import com.grips.scheduler.api.IScheduler;
import com.grips.scheduler.challanges.GraspingScheduler;
import com.grips.scheduler.exploration.ExplorationScheduler;
import com.robot_communication.services.GripsRobotClient;
import lombok.extern.apachecommons.CommonsLog;
import org.dozer.Mapper;
import org.robocup_logistics.llsf_msgs.BeaconSignalProtos;
import org.robocup_logistics.llsf_msgs.GripsStartPositionsProtos;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.function.Consumer;

@Service
@CommonsLog
public class BeaconMsgHandler implements Consumer<BeaconSignalProtos.BeaconSignal> {

    @Value("${gameconfig.scheduler.task_delay}")
    private int SLEEP_TASK_ASSIGNMENT;
    private final RobotConnections robotConnections;
    private final Mapper mapper;
    private final BeaconSignalFromRobotDao beaconSignalFromRobotDao;
    private final RefboxClient refboxClient;
    private final GripsRobotClient robotClient;
    private final DbService dbService;
    private final IScheduler productionScheduler;
    private final ExplorationScheduler explorationScheduler;
    private boolean currentlyInAssignTask = false;

    private final RefboxConfig refboxConfig;
    private final StartPositionsConfig startPositionsConfig;

    @Value("${gameconfig.planningparameters.minorderstoplan}")
    private int minOrdersToPlan;

    private final StationsConfig stationsConfig;
    public BeaconMsgHandler(RobotConnections robotConnections,
                            Mapper mapper,
                            BeaconSignalFromRobotDao beaconSignalFromRobotDao,
                            RefboxClient refboxClient,
                            GripsRobotClient robotClient,
                            DbService dbService,
                            @Qualifier("production-scheduler") IScheduler productionScheduler,
                            ExplorationScheduler explorationScheduler,
                            RefboxConfig refboxConfig,
                            StartPositionsConfig startPositionsConfig,
                            StationsConfig stationsConfig) {
        this.robotConnections = robotConnections;
        this.mapper = mapper;
        this.beaconSignalFromRobotDao = beaconSignalFromRobotDao;
        this.refboxClient = refboxClient;
        this.robotClient = robotClient;
        this.dbService = dbService;
        this.productionScheduler = productionScheduler;
        this.explorationScheduler = explorationScheduler;
        this.refboxConfig = refboxConfig;
        this.startPositionsConfig = startPositionsConfig;
        this.stationsConfig = stationsConfig;
    }

    @Override
    public void accept(BeaconSignalProtos.BeaconSignal beacon_signal) {
        acceptPrivate(beacon_signal);
    }

    private void acceptPrivate(BeaconSignalProtos.BeaconSignal beacon_signal) {
        BeaconSignalFromRobot bSigRobot = mapper.map(beacon_signal, BeaconSignalFromRobot.class);
        bSigRobot.setTaskId(beacon_signal.getTask().getTaskId());
        Key key = RobotMessageRegister.getInstance().get_msg_key_from_class(BeaconSignalProtos.BeaconSignal.class);

        int robotId = 0;
        try {
            robotId = Integer.parseInt(bSigRobot.getRobotId());
        } catch (Exception e) {
            log.error("Robot ID cannot be parsed!");
            return;
        }

        bSigRobot.setLocalTimestamp(System.currentTimeMillis());
        beaconSignalFromRobotDao.save(bSigRobot);

        if (refboxClient.getTeamColor().isEmpty()) {
            log.warn("TeamColor not yet configured");
            return;
        }

        Peer robot = robotConnections.getRobot(robotId);
        if (robot == null) {
            log.error("Robot with ID " + robotId + " not found in connection manager!!!");
            return;
        }

        refboxClient.sendBeaconSignal(robotId,
                beacon_signal.getPeerName(),
                beacon_signal.getPose().getX(),
                beacon_signal.getPose().getY(),
                beacon_signal.getPose().getOri());

        if (!currentlyInAssignTask && PeerState.ACTIVE.equals(robot.getRobotState())) {
            currentlyInAssignTask = true;
            // if a robot sends a beacon signal in exploration, we assign him a task
            // if the robot has no task
            if (GamePhase.Setup.equals(refboxClient.getGamePhase())) {
                handleSetupPhase(beacon_signal, robotId, robot);
            }
            if (GamePhase.Exploration.equals(refboxClient.getGamePhase())
                    && refboxClient.getGameState().equals(GameState.Running)
                    && !robotClient.isRobotsStopped()
                    && (System.currentTimeMillis() - robot.getTimeLastTaskAssignment()) > SLEEP_TASK_ASSIGNMENT) {
                handleExplorationPhase(beacon_signal, robotId, robot);
            }

            // if a robot sends a beacon signal in production, we assign him a task
            // if the robot has no task
            if (GamePhase.Production.equals(refboxClient.getGamePhase())
                    && refboxClient.getGameState().equals(GameState.Running)
                    && !robotClient.isRobotsStopped()
                    && (System.currentTimeMillis() - robot.getTimeLastTaskAssignment()) > SLEEP_TASK_ASSIGNMENT) { //todo this does not use the sim time!
                handleProductionPhase(beacon_signal, robotId, robot);
            }

            currentlyInAssignTask = false;
        }
    }

    private void handleSetupPhase(BeaconSignalProtos.BeaconSignal beacon_signal, int robotId, Peer robot) {
        log.info("Info:\nTeamColor: " + refboxClient.getTeamColor().map(Enum::toString).orElse("NOT CONFIGURED") + "\nCryptoKey: " + refboxConfig.getCrypto_key() +
                "\nAspMinOrders" + minOrdersToPlan);
        refboxClient.getTeamColor().ifPresent(color -> {
            switch (color) {
                case CYAN:
                    sendStartPoseForRobot(robotId, startPositionsConfig.getCyan());
                    break;
                case MAGENTA:
                    sendStartPoseForRobot(robotId, startPositionsConfig.getMagenta());
                    break;
                default:
                    log.warn("Team Color has invalid value: " + color);
            }
        });
    }

    private void sendStartPoseForRobot(int robotId, TeamColor usingTeamConfig) {
        GripsStartPositionsProtos.GripsStartPositions gripsStartPositions;
        switch (robotId) {
            case 1:
                gripsStartPositions = GripsStartPositionsProtos.GripsStartPositions.newBuilder()
                        .setStartpositionX(usingTeamConfig.getRobot1().getX()).
                        setStartpositionY(usingTeamConfig.getRobot1().getY()).
                        setStartpositionYaw(usingTeamConfig.getRobot1().getYaw()).build();
                break;
            case 2:
                gripsStartPositions = GripsStartPositionsProtos.GripsStartPositions.newBuilder()
                        .setStartpositionX(usingTeamConfig.getRobot2().getX()).
                        setStartpositionY(usingTeamConfig.getRobot2().getY()).
                        setStartpositionYaw(usingTeamConfig.getRobot2().getYaw()).build();
                break;
            case 3:
                gripsStartPositions = GripsStartPositionsProtos.GripsStartPositions.newBuilder()
                        .setStartpositionX(usingTeamConfig.getRobot3().getX()).
                        setStartpositionY(usingTeamConfig.getRobot3().getY()).
                        setStartpositionYaw(usingTeamConfig.getRobot3().getYaw()).build();
                break;
            default:
                throw new RuntimeException("Robot ID must be 1,2 or 3");
        }
        robotClient.sendToRobot(robotId, gripsStartPositions);
    }


    private void handleProductionPhase(BeaconSignalProtos.BeaconSignal beacon_signal, int robotId, Peer robot) {
        // check if required information was already received from refbox
        boolean startInExplo = false;
        if (productionScheduler instanceof GraspingScheduler) {
            this.productionScheduler.handleBeaconSignal(beacon_signal, robotId, robot);
        } else {
            if (this.productionScheduler instanceof AspScheduler) {
                //boolean foundAllMachines = true;
                boolean foundAllMachines = (refboxClient.getKnownMachinesCount() >= stationsConfig.countUsedMachines()) || (refboxClient.getLatestGameTimeInSeconds() > 180);
                if (startInExplo && !foundAllMachines) { //todo more smart check here!
                    log.info("HERE: exploration scheduler is working! " + refboxClient.getKnownMachinesCount());
                    explorationScheduler.handleBeaconSignal(beacon_signal, robotId, robot);
                } //if all found, start production!
                else if (dbService.checkProductionReady() && dbService.hasOrders()) {
                    if (startInExplo && explorationScheduler.anyActiveTasks()) {
                        log.warn("Waiting that exploration scheduler tasks finish...");
                        this.explorationScheduler.cancelAllTask();
                    } else if (foundAllMachines) { //number of machines in our teamcolor!
                    // } else if (refboxClient.getKnownMachinesCount() >= stationsConfig.countUsedMachines()) { //number of machines in our teamcolor!
                        productionScheduler.handleBeaconSignal(beacon_signal, robotId, robot);
                    } else {
                        log.warn("SHOULD NOT HAPPEN");
                    }
                } else {
                    log.warn("Not yet ready for production!");
                }
            } else {
                this.productionScheduler.handleBeaconSignal(beacon_signal, robotId, robot);
            }
        }
    }

    private void handleExplorationPhase(BeaconSignalProtos.BeaconSignal beacon_signal, int robotId, Peer robot) {
        explorationScheduler.handleBeaconSignal(beacon_signal, robotId, robot);
    }
}
