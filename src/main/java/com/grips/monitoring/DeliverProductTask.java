package com.grips.monitoring;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DeliverProductTask {
    private Long robotId;
    private String machine;
    private String machinePoint;
}
